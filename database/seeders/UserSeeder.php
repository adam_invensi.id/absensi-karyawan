<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'id' => 1,
            'nama' => 'Adam Syamsinendra',
            'username' => 'Adam',
            'email' => 'adam@invensi.id',
            'email_verified_at' => NULL,
            'password' => '$2y$10$Uo3rbkuUFpyY2lcjKXhjOuGg14Que7QZRPzyairGfKWyeXQ5rDfgi',
            'role_id' => 1,
            'status' => 1,
            'remember_token' => NULL,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 2,
            'nama' => 'Zaidan Aqori Pramana Putra',
            'username' => 'Zaidan',
            'email' => 'zaid@invensi.id',
            'email_verified_at' => NULL,
            'password' => '$2y$10$/uuXWQRQLJYQWrM7Rn1IYej7TxicZ3MT7SCuFWip57ISPdt94/Vfu',
            'role_id' => 2,
            'status' => 1,
            'remember_token' => NULL,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 3, 
            'nama' => 'Wahyu Adi Pramudya', 
            'username' => 'Wahyu', 
            'email' => 'Wahyu@invensi.id', 
            'email_verified_at' => NULL, 
            'password' => '$2y$10$E6rdUfNh0FdXSb0xSb4EXOuR2rwDbIwxyAsm3tqKEPQFV1cb7BNE2', 
            'role_id' => 2, 
            'status' => 1, 
            'remember_token' => NULL, 
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 4,
            'nama' => 'Muhamad Nasoha M',
            'username' => 'Nasuha',
            'email' => 'nasuha@invensi.id',
            'email_verified_at' => NULL,
            'password' => '$2y$10$VI6QizocNi5rj2itswovSuhu.khkSliXzmjRC2iJD9MZYDsf3/MJu',
            'role_id' => 2,
            'status' => 1,
            'remember_token' => NULL,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' =>	 5, 
            'nama' => 'Hendra Rahma Wiartha', 
            'username' => 'Hendra', 
            'email' => 'hendra@invensi.id', 
            'email_verified_at' => NULL, 
            'password' => '$2y$10$stEqXNM64jMue0eeVWEdZOd2aXcPa0exD5/F2y.7.eTxD1P.i/AQ2', 
            'role_id' => 2, 
            'status' => 1, 
            'remember_token' => NULL, 
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 6,
            'nama' => 'Anggia Rizkika Hanin',
            'username' => 'Anggia',
            'email' => 'anggia@invensi.id',
            'email_verified_at' => NULL,
            'password' => '$2y$10$TihtLrPA9rZyNmI4WPSYAen2s1R7uNHsunwGYWcF2rUvwCJt9jJdS',
            'role_id' => 2,
            'status' => 1,
            'remember_token' => NULL,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 7,
            'nama' => 'M. Fitranu Akbar Afda',
            'username' => 'Akbar',
            'email' => 'akbar@invensi.id',
            'email_verified_at' => NULL,
            'password' => '$2y$10$/bKFCIB3/PYD2O5l4JTSNeWj0GSjmZ/sty7QfyIcsRLvCDs6yHU02',
            'role_id' => 2,
            'status' => 1,
            'remember_token' => NULL,
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}