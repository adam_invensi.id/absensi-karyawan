<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMposisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_posisi')->insert([[
            'posisi' => "Chief Executive Officer",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'posisi' => "Chief Information Officer",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'posisi' => "Direktur",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'posisi' => "HR",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'posisi' => "Karyawan",
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
