<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMupahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_upah')->insert([[
            'upah' => 1000000,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'upah' => 1500000,
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'upah' => 2000000,
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
