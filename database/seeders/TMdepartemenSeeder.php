<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMdepartemenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_departemen')->insert([[
            'departemen' => "Departemen Teknologi",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'departemen' => "Departemen Operasional",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'departemen' => "Departemen Sales",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'departemen' => "Departemen FAT dan HRDE",
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
