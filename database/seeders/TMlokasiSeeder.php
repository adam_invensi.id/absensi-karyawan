<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMlokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_lokasi')->insert([[
            'lokasi' => "Work From Office",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'lokasi' => "Work From Home",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'lokasi' => "Visit Costumer",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'lokasi' => "Lainnya...",
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
