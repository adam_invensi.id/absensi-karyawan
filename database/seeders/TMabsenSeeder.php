<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMabsenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_absen')->insert([[
            'absen' => "Masuk",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'absen' => "Pulang",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'absen' => "Izin",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'absen' => "Sakit",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'absen' => "Pulang Cepat",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'absen' => "Cuti",
            'created_at' => date("Y-m-d H:i:s"),
        ]]);
    }
}
