<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMjabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_jabatan')->insert([[
            'departemen_id' => 1,
            'jabatan' => "Senior",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'departemen_id' => 1,
            'jabatan' => "Junior",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'departemen_id' => 1,
            'jabatan' => "Staff Support Project",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'departemen_id' => 2,
            'jabatan' => "Junior",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'departemen_id' => 4,
            'jabatan' => "Staff Accounting",
            'created_at' => date("Y-m-d H:i:s")
        ],[ 
            'departemen_id' => 4,
            'jabatan' => "Staff HRDE",
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
