<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TMabsenSeeder::class);
        $this->call(TMdepartemenSeeder::class);
        $this->call(TMlokasiSeeder::class);
        $this->call(TMjabatanSeeder::class);
        $this->call(TMroleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(BiodataSeeder::class);
        $this->call(JamKerjaSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
