<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JamKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_jam_kerja')->insert([[
            'jam' => "Masuk",
            'jam_mulai' => "08:00:00",
            'jam_selesai' => "08:15:00",
            'created_at' => date("Y-m-d H:i:s"),
        ],[
            'jam' => "Pulang",
            'jam_mulai' => "17:00:00",
            'jam_selesai' => "17:15:00",
            'created_at' => date("Y-m-d H:i:s"),
        ]]);
    }
}
