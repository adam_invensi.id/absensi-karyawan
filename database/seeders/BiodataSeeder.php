<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BiodataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_biodata')->insert([[
            'id' => 1,
            'user_id' => 1,
            'jabatan_id' => 2,
            'npwp' => '71.903.247.6-438.000',
            'tmk' => '01-03-2021',
            'no_telepon' => '089519232223',
            'jenis_kelamin' => 'Laki-Laki',
            'tempat_lahir' => 'Ngawi',
            'tanggal_lahir' => '2001-12-20',
            'alamat' => 'Bukit Cimanggu Greenland Residence, Blok Hh 7 No.10, RT.2/RW.11, Sukadamai, Tanah Sereal',
            'agama' => 'Islam',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 2,
            'user_id' => 2,
            'jabatan_id' => 2,
            'npwp' => '77.172.293.1-412.000',
            'tmk' => '01-03-2021',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 3,
            'user_id' => 3,
            'jabatan_id' => 2,
            'npwp' => '59.094.740.4-001.000',
            'tmk' => '01-06-2021',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 4,
            'user_id' => 4,
            'jabatan_id' => 2,
            'npwp' => '24.688.683.2-005.000',
            'tmk' => '06-12-2021',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 5,
            'user_id' => 5,
            'jabatan_id' => 1,
            'npwp' => '48.015.312.1-416.000',
            'tmk' => '03-01-2022',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 6,
            'user_id' => 6,
            'jabatan_id' => 3,
            'npwp' => '17.341.567.0-024.000',
            'tmk' => '01-07-2022',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'id' => 7,
            'user_id' => 7,
            'jabatan_id' => 3,
            'npwp' => '96.012.779.3-448.000',
            'tmk' => '02-07-2022',
            'no_telepon' => '-',
            'jenis_kelamin' => '-',
            'tempat_lahir' => '-',
            'tanggal_lahir' => NULL,
            'alamat' => '-',
            'agama' => '-',
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}