<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TMroleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tm_role')->insert([[
            'role' => "Administrator",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'role' => "Karyawan",
            'created_at' => date("Y-m-d H:i:s")
        ],[
            'role' => "HR",
            'created_at' => date("Y-m-d H:i:s")
        ]]);
    }
}
