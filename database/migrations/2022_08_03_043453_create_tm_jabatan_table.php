<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmJabatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_jabatan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('departemen_id')->index();
            $table->string('jabatan');
            $table->timestamps();

            $table->foreign('departemen_id')->references('id')->on('tm_departemen')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_jabatan');
    }
}
