<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalAbsensiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_absensi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('tanggal', 50)->nullable();
            $table->integer('jmlh_hari')->default('0');
            $table->integer('jmlh_jam')->default('0');
            $table->integer('jmlh_sakit')->default('0');
            $table->integer('jmlh_cuti')->default('0');
            $table->integer('jmlh_izin')->default('0');
            $table->integer('jmlh_alpha')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_absensi');
    }
}
