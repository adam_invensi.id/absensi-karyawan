<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmBiodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_biodata', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('jabatan_id');
            $table->string('nik')->nullable();
            $table->string('npwp')->nullable();
            $table->string('tmk')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('alamat', 1000)->nullable();
            $table->string('agama')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_biodata');
    }
}
