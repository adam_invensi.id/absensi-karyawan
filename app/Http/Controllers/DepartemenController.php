<?php

namespace App\Http\Controllers;

use App\Models\TMdepartemen;
use Illuminate\Http\Request;

class DepartemenController extends Controller
{
    public function index()
    {
        $tmDepartemen=TMdepartemen::get();
        return view('departemen.index', compact('tmDepartemen'));
    }

    public function store(Request $request)
    {
        $response = array(
            'status' => 'failed'
        );
        $insert = TMdepartemen::create([
            'departemen' => $request->departemen,
        ]);
        if ($insert) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function updateDepartemen(Request $request)
    {
        $id = $request->id;
        $departemen = $request->name;

        $updateDepartemen = TMdepartemen::find($id);
        $updateDepartemen->departemen=$departemen;
        $updateDepartemen->save();

        if ($updateDepartemen) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function delete($id)
    {
        $data =  TMdepartemen::find($id)->delete($id);

        return response()->json(['message' => 'Record deleted successfully!']);
    }
}
