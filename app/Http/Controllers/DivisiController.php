<?php

namespace App\Http\Controllers;

use App\Models\TMdepartemen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DivisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tmDepartemen=TMdepartemen::get();
        return view('divisi.index', compact('tmDepartemen'));
    }

    public function store(Request $request)
    {
        $response = array(
            'status' => 'failed'
        );
        $insert = TMdepartemen::create([
            'jabatan' => $request->divisi,
        ]);
        if ($insert) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function updateDivisi(Request $request)
    {
        $id = $request->id;
        $jabatan = $request->name;

        $updateDepartemen = TMdepartemen::find($id);
        $updateDepartemen->jabatan=$jabatan;
        $updateDepartemen->save();

        if ($updateDepartemen) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function delete($id)
    {
        $data =  TMdepartemen::find($id)->delete($id);

        return response()->json(['message' => 'Record deleted successfully!']);
    }
}
