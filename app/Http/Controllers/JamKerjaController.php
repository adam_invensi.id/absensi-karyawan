<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TMjamkerja;
use Illuminate\Http\Request;

class JamKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tmjamkerja=TMjamkerja::get();
        return view('jam_kerja.index', compact('tmjamkerja'));
    }

    public function updateJam(Request $request)
    {
        $id = $request->id;
        $keterangan = $request->keterangan;
        $jam_mulai = $request->mulai;
        $jam_selesai = $request->selesai;

        $updateJam = TMjamkerja::find($id);
        $updateJam->jam=$keterangan;
        $updateJam->jam_mulai=$jam_mulai;
        $updateJam->jam_selesai=$jam_selesai;
        $updateJam->save();

        if ($updateJam) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }
}
