<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\TMabsen;
use App\Models\TMjamkerja;
use App\Models\TMlokasi;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {
        $absen = TMabsen::get();
        $date = date("Y-m-d");
        $absensi_pulang = Absensi::where('absen_id', '=', 2)->where('user_id', '=', auth()->user()->id)->where('tanggal_absen', '=', $date)->first();
        $absensi_masuk = Absensi::where('absen_id', '=', 1)->where('user_id', '=', auth()->user()->id)->where('tanggal_absen', '=', $date)->first();
        $lokasi = TMlokasi::get();
        $waktu = TMjamkerja::get();
        
        
        return view('dashboard.index', compact('absen', 'lokasi', 'waktu', 'absensi_pulang', 'absensi_masuk'));
    }
}
