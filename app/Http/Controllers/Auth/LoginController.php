<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\TMabsen;
use App\Models\TMlokasi;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ],[
            'email.required' => 'Harap isi bidang Email',
            'email.email' => 'Email tidak valid',
            'password.required' => 'Harap isi bidang Password',
        ]); 

        if (Auth::Attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
        return back()->with('loginError', 'Email atau password salah.');
    }

    public function showLoginForm()
    {
        $absen = TMabsen::all();
        $lokasi = TMlokasi::all();
        return view('auth.login', compact('absen', 'lokasi'));
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
