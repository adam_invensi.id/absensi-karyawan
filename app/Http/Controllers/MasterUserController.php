<?php

namespace App\Http\Controllers;

use App\Models\TMbiodata;
use App\Models\TMdepartemen;
use App\Models\TMjabatan;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TMrole;
use App\Models\TMupah;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use stdClass;

class MasterUserController extends Controller
{
    public function index()
    {
        $role = TMrole::get();
        $users = User::select('users.*', 'tm_role.role')
                    ->join('tm_role', 'users.role_id', '=', 'tm_role.id')
                    ->get();
        $jabatan = TMjabatan::select('tm_jabatan.*', 'tm_departemen.departemen')->join('tm_departemen', 'tm_departemen.id', 'tm_jabatan.departemen_id')->orderBy('departemen_id', 'ASC')->get();
        $departemen = TMdepartemen::get();
        $upah = TMupah::get();
        return view('master_user.index', compact('users', 'role', 'jabatan', 'departemen', 'upah'));
    }

    public function create()
    {
        return view('master_user.create');
    }

    public function store(Request $request)
    {
        
        // dd($request->role);
        $response = array(
            'status' => 'failed'
        );
        $validated = Validator::make($request->all(),[
            'email' => 'required|email|unique:users',
            'nama' => 'required',
            'name' => 'required',
            'password' => 'required|min:5',
            'role' => 'required',
            'departemen' => 'required',
            'upah' => 'required',
            'nik' => 'required',
            'npwp' => 'required',
            'tmk' => 'required',
            'no_telepon' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'agama' => 'required',
        ],[
            'email.required' => 'Harap isi bidang Email',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email Sudah Ada',
            'nama.required' => 'Harap isi bidang Nama Lengkap',
            'name.required' => 'Harap isi bidang Username',
            'password.required' => 'Harap isi bidang Password',
            'password.min' => 'Password minimal 3',
            'role.required' => 'Harap isi bidang Peran/Role',
            'departemen.required' => 'Harap isi bidang Departemen',
            'upah.required' => 'Harap memilih bidang Upah',
            'nik.required' => 'Harap isi bidang NIK',
            'npwp.required' => 'Harap isi bidang NPWP',
            'tmk.required' => 'Harap isi bidang Tanggal Masuk Kerja',
            'no_telepon.required' => 'Harap isi bidang Nomor Telepon',
            'tempat_lahir.required' => 'Harap isi bidang Tempat Lahir',
            'tanggal_lahir.required' => 'Harap isi bidang Tanggal Lahir',
            'alamat.required' => 'Harap isi bidang Alamat',
            'agama.required' => 'Harap isi bidang Agama',
        ]);
        if ($validated->fails())
        {
            return response()->json(['errors'=>$validated->errors()->all()]);
        }
        // dd($request->jenis_kelamin);
        if ($validated) {
            $insert = User::create([
                'nama' => $request->nama,
                'username' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role_id' => $request->role,
                'status' => ($request->status == 'on') ? 1 : 0,
            ]);
            $last_inserted_id = User::latest()->first()->id;
            $insert_biodata = TMbiodata::create([
                'user_id' => $last_inserted_id,
                'jabatan_id' => $request->departemen,
                'upah_id' => $request->upah,
                'nik' => $request->nik,
                'npwp' => $request->npwp,
                'tmk' => $request->tmk,
                'no_telepon' => $request->no_telepon,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'alamat' => $request->alamat,
                'agama' => $request->agama,
            ]);
            
            if($insert_biodata){
                if ($insert) {
                    $response = array(
                        'status' => 'success'
                    );
                }else{
                    $response = array(
                        'status' => 'failed'
                    );
                }
            }else {
                $response = array(
                    'status' => 'failed'
                );
            }
        }
        echo json_encode($response);
    }

    public function show($id)
    {

        $user = User::select('users.*', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.nik','tm_biodata.npwp', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_biodata.status_perkawinan', 'tm_jabatan.departemen_id','tm_jabatan.jabatan', 'tm_departemen.departemen', 'tm_upah.upah')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->join('tm_upah', 'tm_biodata.upah_id', '=', 'tm_upah.id')
        ->where('users.id', $id)
        ->first();
        
        return view('master_user.show', compact('user'));
    }

    public function delete($id)
    {
        $deleteUser =  User::find($id)->delete($id);
        

        return response()->json(['message' => 'Record deleted successfully!']);
    }

    public function pengaturanAkun($id)
    {
        // $user = User::select('users.*', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.departemen_id', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
        // ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        // ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        // ->join('tm_departemen', 'tm_biodata.departemen_id', '=', 'tm_departemen.id')
        // ->where('users.id', $id)
        // ->first();
        // $jabatan =  $role = TMjabatan::get();
        // $departemen =  $role = TMdepartemen::get();

        // return view('master_user.pengaturan_akun', compact('user', 'jabatan', 'departemen'));
    }

    

    public function pengaturanPengguna($id)
    {
        $user = User::select('users.*', 'tm_biodata.id as biodata_id', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.upah_id', 'tm_biodata.npwp', 'tm_biodata.nik', 'tm_biodata.tmk', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.status_perkawinan', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.departemen_id', 'tm_jabatan.jabatan', 'tm_departemen.departemen', 'tm_upah.upah')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->join('tm_upah', 'tm_biodata.upah_id', '=', 'tm_upah.id')
        ->where('users.id', $id)
        ->first();
        
        $jabatan =  TMjabatan::get();
        $departemen =  TMdepartemen::get();
        $upah =  TMupah::get();
        
        return view('master_user.pengaturan_pengguna', compact('user', 'jabatan', 'departemen', 'upah'));
    }

    public function updatePengaturanPengguna(Request $request)
    {
        // dd($request->oldPhoto);
        // return $request->file('photo')->store('post-images');
        $user = User::where('id', $request['user_id'])->first();
        if ($user) {
            if ($request->file('photo')) {
                
                if ($request->has('nama') && $request->has('username') ) {
                    if ($request->oldPhoto) {
                        Storage::delete('public/'.$request->oldPhoto);
                    }
                    $updateUser = User::find($request->user_id);
                    $updateUser->nama=$request['nama'];
                    $updateUser->username=$request['username'];
                    $updateUser->photo=$request->file('photo')->store('post-images', 'public');
                    $updateUser->save();
                }
                $updateBiodata = TMbiodata::find($request->biodata_id);
                $updateBiodata->jabatan_id=$request['jabatan'];
                $updateBiodata->upah_id=$request['upah'];
                $updateBiodata->no_telepon=$request['no_telepon'];
                $updateBiodata->tanggal_lahir=$request['tanggal_lahir'];
                $updateBiodata->tempat_lahir=$request['tempat_lahir'];
                $updateBiodata->jenis_kelamin=$request['jenis_kelamin'];
                $updateBiodata->alamat=$request['alamat'];
                $updateBiodata->status_perkawinan=$request['status_perkawinan'];
                $updateBiodata->tmk=$request['tmk'];
                $updateBiodata->nik=$request['nik'];
                $updateBiodata->save();
            }else {
                if ($request->has('nama') && $request->has('username') ) {
                    $updateUser = User::find($request->user_id);
                    $updateUser->nama=$request['nama'];
                    $updateUser->username=$request['username'];
                    $updateUser->save();
                }
                $updateBiodata = TMbiodata::find($request->biodata_id);
                $updateBiodata->jabatan_id=$request['jabatan'];
                $updateBiodata->upah_id=$request['upah'];
                $updateBiodata->no_telepon=$request['no_telepon'];
                $updateBiodata->tanggal_lahir=$request['tanggal_lahir'];
                $updateBiodata->tempat_lahir=$request['tempat_lahir'];
                $updateBiodata->jenis_kelamin=$request['jenis_kelamin'];
                $updateBiodata->alamat=$request['alamat'];
                $updateBiodata->status_perkawinan=$request['status_perkawinan'];
                $updateBiodata->tmk=$request['tmk'];
                $updateBiodata->nik=$request['nik'];
                $updateBiodata->save();
            }
            if ($updateBiodata) {
                return redirect()->back()->with('success', 'Perubahan sukses!');
            }else {
                return redirect()->back()->with('failed', 'Perubahan gagal.');
            }
        }else{
            return redirect()->back()->with('failed', 'Email Tidak ada!');
        }
        return redirect()->back()->with('failed', 'Perubahan gagal.');
    }

    public function updatePengaturanAkun(Request $request, $id)
    {
        
    }

    public function changePassword($id)
    {
        $user = User::select('users.*', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.npwp', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.departemen_id', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
            ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
            ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
            ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
            ->where('users.id', $id)
            ->first();
        return view('master_user.change_password', compact('user'));
    }

    public function updatePassword(Request $request)
    {
        $response = [
            'status' => 'errors',
            'message' => 'Penggantian password gagal!'
        ];
        ini_set('memory_limit', '1024M');
        $request->validate([
            'old_password' => 'required',
        ], [
            'old_password.required' => 'Mohon untuk mengisi Password lama',
        ]);

        $user = User::select('users.*')
            ->where('users.id', $request->user_id)
            ->first();
        $hash = Hash::check($request->old_password, $user->password);
        if ($hash == 'true') {
            $validate = $request->validate([
                'new_password' => 'required',
                'new_password' => 'min:4',
                'new_password' => 'different:old_password',
                'confirmation_password' => 'required',
                'confirmation_password' => 'min:4',
                'confirmation_password' => 'same:new_password',
            ], [
                'new_password.required' => 'Mohon untuk mengisi password baru',
                'new_password.min' => 'Minimal 4 karakter',
                'confirmation_password.same' => "Password Konfirmasi tidak sama",
                'confirmation_password.required' => 'Mohon untuk mengisi password Konfirmasi',
                'confirmation_password.min:4' => 'Minimal 4 karakter',
            ]);
            
            if ($validate['confirmation_password'] == null || $validate['new_password'] == null) {
                $merge = ($validate['new_password'] == null) ? ' Password baru' : '';
                $merge .= ($validate['confirmation_password'] == null && $validate['new_password'] == null) ? ' &' : '';
                $merge .= ($validate['confirmation_password'] == null) ? ' Konfirmasi password' : '';
                $response = [
                        'status' => 'errors',
                        'message' => 'Mohon untuk mengisi'. $merge
                    ];
                return response()->json($response);
            }   

            if($validate){
                User::where('id', $request->user_id)
                    ->update([
                        'password' => Hash::make($request->confirmation_password)
                    ]);
                $response = [
                    'status' => 'true',
                    'message' => 'Penggantian password berhasil!'
                ];
            }
            return response()->json($response);
            
        } else {
            $response = [
                'status' => 'errors',
                'message' => 'Password lama salah'
            ];
            return response()->json($response);
        }
    }
}
