<?php

namespace App\Http\Controllers;

use App\Models\TMdepartemen;
use App\Models\TMjabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    public function index()
    {
        $tmJabatan=TMjabatan::select('tm_jabatan.*', 'tm_departemen.departemen')->join('tm_departemen', 'tm_departemen.id', 'tm_jabatan.departemen_id' )->get();
        $tmDepartemen=TMdepartemen::get();
        return view('jabatan.index', compact('tmJabatan', 'tmDepartemen'));
    }

    public function store(Request $request)
    {
        
        $response = array(
            'status' => 'failed'
        );
        $insert = TMjabatan::create([
            'departemen_id' => $request->departemen,
            'jabatan' => $request->jabatan,
        ]);
        if ($insert) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function updateJabatan(Request $request)
    {
        $id = $request->id;
        $departemen = $request->departemen;
        $jabatan = $request->jabatan;

        $updateJabatan = TMjabatan::find($id);
        $updateJabatan->departemen_id=$departemen;
        $updateJabatan->jabatan=$jabatan;
        $updateJabatan->save();

        if ($updateJabatan) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function delete($id)
    {
        $data =  TMjabatan::find($id)->delete($id);

        return response()->json(['message' => 'Record deleted successfully!']);
    }
}
