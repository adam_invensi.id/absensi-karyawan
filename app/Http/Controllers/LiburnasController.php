<?php

namespace App\Http\Controllers;

use App\Models\TMliburnas;
use DOMDocument;
use Illuminate\Http\Request;

class LiburnasController extends Controller
{
    public function index()
    {
        $tmlibur = TMliburnas::get();
        $liburnas = $this->getHolidays();
        // dd($liburnas);
        return view('liburnas.index', compact('liburnas', 'tmlibur'));
    }
    
    public function storeLiburnasFromWebsite(Request $request)
    {
        $response = array(
            'status' => 'failed'
        );
        $liburnas = $this->getHolidays();
        
        foreach ($liburnas as $key => $value) {
            foreach ($request->libur as $indeks => $v) {
                if ($key == $indeks) {
                    $rawTanggal = explode(" ", $value[0]);
                    $bulan = $this->convertbulaninggris($rawTanggal[1]);
                    $insert = TMliburnas::create([
                        'tanggal' => date('Y-m-d', strtotime($rawTanggal[0] ."-".$bulan)),
                        'keterangan' => $value[2]
                    ]);
                    if ($insert) {
                        $response = array(
                            'status' => 'success'
                        );
                    }else{
                        $response = array(
                            'status' => 'failed'
                        );
                    }
                }
            }
        }
        echo json_encode($response);
    }

    public function updateLibur(Request $request)
    {
        $id = $request->id;
        $tanggal = $request->tanggal;
        $keterangan = $request->keterangan;

        $updateLiburnas = TMliburnas::find($id);
        $updateLiburnas->tanggal=$tanggal;
        $updateLiburnas->keterangan=$keterangan;
        $updateLiburnas->save();

        if ($updateLiburnas) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function store(Request $request)
    {
        $insert = TMliburnas::create([
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);
        if ($insert) {
            $response = array(
                'status' => 'success'
            );
        }else{
            $response = array(
                'status' => 'failed'
            );
        }
        echo json_encode($response);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function delete($id)
    {
        $data =  TMliburnas::find($id)->delete($id);

        return response()->json(['message' => 'Record deleted successfully!']);
    }

    public static function getHolidays(){
        $url='https://publicholidays.co.id/id/'. date('Y') .'-dates/';
        $resp = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        // Load HTML to DOM Object
        $dom = new DOMDocument();
        @$dom->loadHTML($data);
        
        $holidays=array();
            $items = $dom->getElementsByTagName('tr');
            
            function tdrows($elements)
            {
                $str = "";
                foreach ($elements as $element)
                {
                    $str .= $element->nodeValue . ", ";
                }
                //This pplaces the items into an array 
                $tempArray=explode(',',$str);
                //This gets rid of empty array elements
                unset($tempArray[4]);
                unset($tempArray[5]);
                return $tempArray;
            }
            foreach ($items as $node)
            {
                $holidays[]=tdrows($node->childNodes);
                
            }
        //The first and secone items in the array were the titles of the table and a blank row 
        //so we unset them
        unset($holidays[0]);
        foreach ($holidays as $key => $value) {
            if(count($value) == 4){
                if ($value[3] != " ") {
                    unset($holidays[$key]);
                }
                
            }else{
                unset($holidays[$key]);
            }
            $explode = explode(" ", $value[0]);
            if(array_search("to", $explode)){
                unset($holidays[$key]);
            }
            unset($holidays[$key][3]);
        }
        $holidays = array_values($holidays);
        return $holidays;
    }

    public static function convertbulaninggris($bulan)
    {
        switch ($bulan) {
            case 'Januari':
            return $bulan = 'January';
            case 'Februari':
            return $bulan = 'February';
            case 'Maret':
            return $bulan = 'March';
            case 'April':
            return $bulan = 'April';
            case 'Mei':
            return $bulan = 'May';
            case 'Juni':
            return $bulan = 'June';
            case 'Juli':
            return $bulan = 'July';
            case 'Agustus':
            return $bulan = 'August';
            case 'September':
            return $bulan = 'September';
            case 'Oktober':
            return $bulan = 'October';
            case 'November':
            return $bulan = 'November';
            case 'Desember':
            return $bulan = 'December';
            default:
            return $bulan = 'Bulan tidak valid';
        }
    }
}
