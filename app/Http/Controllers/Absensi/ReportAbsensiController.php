<?php

namespace App\Http\Controllers\Absensi;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Absensi;
use App\Models\TMliburnas;
use App\Models\Total_absensi;
use App\Models\TTexcel;
use App\Models\User;
use Carbon\CarbonPeriod;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReaderXlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportAbsensiController extends Controller
{
    public function index()
    {
        $ttexcel = TTexcel::first();
        // dd($ttexcel);
        return view('absensi.report.index', compact('ttexcel'));
    }

    public function getDownload()
    {
        $file_path= public_path() ."/assets/excel/Contoh_format.xlsx";
        return response()->download($file_path);
    }

    public function exportReport(Request $request)
    {
        $request->validate([
            'dari_tanggal' => 'required',
            'sampai_tanggal' => 'required',
            'hari' => 'required'
        ]);
        $dari_tanggal = new DateTime(date('Y-m-d', strtotime($request->dari_tanggal . "-1 days")));
        $sampai_tanggal = new DateTime(date('Y-m-d', strtotime($request->sampai_tanggal . "+1 days")));
        if ($request->sampai_tanggal > date('Y-m-d')) {
            $request->sampai_tanggal = date('Y-m-d');
        }
        // dd($dari_tanggal->format('Y-m-d'), $sampai_tanggal->format('Y-m-d'));
        $period = new DatePeriod(
            new DateTime(date('Y-m-d', strtotime($request->dari_tanggal))),
            new DateInterval('P1D'),
            new DateTime(date('Y-m-d',strtotime($request->sampai_tanggal)))
        );
        // dd($period);
        
        $totalHari = $this->sumDaysBetweenDate(0, $request->dari_tanggal, $sampai_tanggal, $request->hari);
        // dd($totalHari);
        $tanggal_awal = $this->convertbulan(date('m', strtotime($request->dari_tanggal))) . "_" . date('Y', strtotime($request->dari_tanggal));
        $tanggal_akhir = $this->convertbulan(date('m', strtotime($request->sampai_tanggal))) . "_" . date('Y', strtotime($request->sampai_tanggal));
        $namaFile = "report_absensi_$tanggal_awal-$tanggal_akhir";
        
        $spreadsheet = new Spreadsheet();
        $html = new \PhpOffice\PhpSpreadsheet\Helper\Html();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(25);
        $spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(26);
        $spreadsheet->getActiveSheet()->getRowDimension('16')->setRowHeight(26);
        
        $styleArrayTitleContent = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $styleArrayTitle = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $styleArrayContent = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];
        $sheet->getStyle("D6:D8")->applyFromArray($styleArrayTitle);
        $sheet->getStyle("D15:P16")->applyFromArray($styleArrayTitleContent);
        $sheet->setCellValue('D6', 'PT. Invensi Digital Nusantara');
        $sheet->setCellValue('D7', 'Rekapitulasi Absensi Karyawan');
        $sheet->setCellValue('D8', 'Periode : '. date('d', strtotime($request->dari_tanggal)). " " .$this->convertbulan(date('m', strtotime($request->dari_tanggal))). " " . date('Y', strtotime($request->dari_tanggal)). ' - '. date('d', strtotime($request->sampai_tanggal)). " " .$this->convertbulan(date('m', strtotime($request->sampai_tanggal))). " " . date('Y', strtotime($request->sampai_tanggal)));
        $sheet->mergeCells('D9:E9')->setCellValue('D9', ($request->hari == 1) ? "Hari Kerja Senin - Jum'at" : "Hari Kerja Senin - Sabtu");
        $sheet->mergeCells('D10:E10')->setCellValue('D10', "Jumlah Hari Kerja ");
        $sheet->mergeCells('D11:E11')->setCellValue('D11', "Jumlah Jam Kerja/Hari ");
        $sheet->mergeCells('D12:E12')->setCellValue('D12', "Jumlah Jam Kerja/Minggu ");
        $sheet->mergeCells('D13:E13')->setCellValue('D13', "Jumlah Jam Kerja/Bulan ");
        $sheet->setCellValue('F9', ($request->hari == 1) ? " : 5 hari" : " : 6 Hari");
        $sheet->setCellValue('F10', " : $totalHari Hari");
        $sheet->setCellValue('F11', ($request->hari == 1) ? " : 8 Jam" : " : 7 Jam");
        $sheet->setCellValue('F12', " : " . ((($request->hari == 1) ? 5 : 6)*(($request->hari == 1) ? 8 : 7)). " Jam");
        $sheet->setCellValue('F13', " : " . ($totalHari*(($request->hari == 1) ? 8 : 7)). " Jam");
        
        $sheet->mergeCells('D15:D16')->setCellValue('D15', 'No.');
        $sheet->mergeCells('E15:E16')->setCellValue('E15', 'Nama Karyawan');
        $sheet->mergeCells('F15:F16')->setCellValue('F15', 'Tanggal Masuk Kerja');
        $sheet->mergeCells('G15:G16')->setCellValue('G15', 'Upah');
        $sheet->mergeCells('H15:H16')->setCellValue('H15', 'Cuti');
        $sheet->mergeCells('I15:I16')->setCellValue('I15', 'Alpa');
        $sheet->mergeCells('J15:J16')->setCellValue('J15', 'Sakit');
        $sheet->mergeCells('K15:K16')->setCellValue('K15', 'Izin');
        $sheet->mergeCells('L15:L16')->setCellValue('L15', 'Jumlah Hadir');
        $sheet->mergeCells('M15:M16')->setCellValue('M15', 'Jumlah Jam Kerja');
        $sheet->mergeCells('N15:O15')->setCellValue('N15', 'Potongan');
        $sheet->setCellValue('N16', "Tidak Hadir");
        $sheet->setCellValue('O16', "Terlambat");
        $sheet->mergeCells('P15:P16')->setCellValue('P15', "Jumlah Potongan");
        
        $user = User::select('users.*', 'tm_biodata.npwp','tm_biodata.tmk', 'tm_upah.upah')->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')->join('tm_upah', 'tm_biodata.upah_id', '=', 'tm_upah.id')->where('users.role_id', 2)->orderBy('tm_biodata.user_id')->get();
        // dd($user);
        $no_cell=1;
        $cellNumber = 17;
        foreach ($user as $key => $value) {
            $totalHari = $this->sumDaysBetweenDate(0, $request->dari_tanggal, $sampai_tanggal, $request->hari);

            $absensiTelat = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang', 'keterangan')
            ->where('user_id', $value->id)
            ->whereRaw("jam_masuk > '08:20:00'")
            ->whereRaw("DATE(tanggal_absen) >= '". $request->dari_tanggal ."' AND DATE(tanggal_absen) <= '". $request->sampai_tanggal . "'")
            ->get();

            $absensi = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang', 'keterangan')
            ->where('user_id', $value->id)
            ->whereRaw("DATE(tanggal_absen) >= '". $request->dari_tanggal ."' AND DATE(tanggal_absen) <= '". $request->sampai_tanggal . "'")
            ->get();
            
            $absensiIzinSakitCuti = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang', 'keterangan')
            ->where('user_id', $value->id)
            ->whereIn('absen_id', [3,4,6])
            ->whereRaw("DATE(tanggal_absen) >= '". $request->dari_tanggal ."' AND DATE(tanggal_absen) <= '". $request->sampai_tanggal . "'")
            ->get();

            $absensiHanyaMasuk = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang', 'keterangan')
            ->where('user_id', $value->id)
            ->where('jam_pulang', NULL)
            ->whereRaw("DATE(tanggal_absen) >= '". $request->dari_tanggal ."' AND DATE(tanggal_absen) <= '". $request->sampai_tanggal . "'")
            ->get();
            
            $totalMenitTerlambat = 0;
            foreach ($absensiTelat as $indeks => $abs) {
                if ($abs->jam_masuk > '10:20:00') {
                    $jam_masuk = strtotime('10:20:00');
                }else {
                    $jam_masuk = strtotime($abs->jam_masuk);
                }
                $batas_masuk = strtotime('08:20:00');
                $totalMenitTerlambat = $totalMenitTerlambat+(($jam_masuk-$batas_masuk)/60);
            }
            // dd(array_column($absensinontelat->toArray(), 'tanggal_absen'));
            $tglTidakHadir = [];
            $tglAlpha = [];
            foreach ($period as $key => $per) {
                $arrayTglAbsensi = array_column($absensi->toArray(), 'tanggal_absen');
                $arrayTglHanyaMasukAbsensi = array_column($absensiHanyaMasuk->toArray(), 'tanggal_absen');
                $arrayTglIzinSakitCuti = array_column($absensiIzinSakitCuti->toArray(), 'tanggal_absen');
                $arrayTglIzin = array_column($absensiIzinSakitCuti->where('absen_id', 3)->toArray(), 'tanggal_absen');
                $arrayTglSakit = array_column($absensiIzinSakitCuti->where('absen_id', 4)->toArray(), 'tanggal_absen');
                $arrayTglCuti = array_column($absensiIzinSakitCuti->where('absen_id', 6)->toArray(), 'tanggal_absen');
                
                if(!in_array($per->format('Y-m-d'), $arrayTglAbsensi)){
                    $weekDay = date('w', strtotime($per->format('Y-m-d')));
                    if ($request->hari == 1) {
                        if(!in_array($weekDay, [0, 6])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                            $tglAlpha[] = $per->format('Y-m-d');
                        }
                    }elseif($request->hari == 2){
                        if(!in_array($weekDay, [0])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                            $tglAlpha[] = $per->format('Y-m-d');
                        }
                    }
                }
                
                if(in_array($per->format('Y-m-d'), $arrayTglHanyaMasukAbsensi)){
                    $weekDay = date('w', strtotime($per->format('Y-m-d')));
                    if ($request->hari == 1) {
                        if(!in_array($weekDay, [0, 6])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                        }
                    }elseif($request->hari == 2){
                        if(!in_array($weekDay, [0])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                        }
                    }
                }
                
                if(in_array($per->format('Y-m-d'), $arrayTglIzinSakitCuti)){
                    $weekDay = date('w', strtotime($per->format('Y-m-d')));
                    if ($request->hari == 1) {
                        if(!in_array($weekDay, [0, 6])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                        }
                    }elseif($request->hari == 2){
                        if(!in_array($weekDay, [0])){
                            $tglTidakHadir[] = $per->format('Y-m-d');
                        }
                    }
                }
            }
            $totalTerlambat = floor($totalMenitTerlambat / 60).' Jam '.($totalMenitTerlambat -   floor($totalMenitTerlambat / 60) * 60). ' Menit';
            
            $totalHari = $totalHari-(count($tglAlpha)+count($arrayTglSakit)+count($arrayTglIzin)+count($absensiHanyaMasuk));
            $totalJam = ($totalHari*(($request->hari == 1) ? 8 : 7));
            $rumusPotongan = ((count($tglTidakHadir)/$totalHari)*$value->upah)+(($totalMenitTerlambat/60)*30000);
            $hasil_rupiah = "Rp " . number_format($value->upah,2,',','.');
            $hasilPotongan = "Rp " . number_format(floor($rumusPotongan),2,',','.');
            
            if ($value != null) {
                $sheet->setCellValue("D$cellNumber", $no_cell);
                $sheet->setCellValue("E$cellNumber", $value->nama);
                $sheet->setCellValue("F$cellNumber", date('d/m/Y', strtotime($value->tmk)));
                $sheet->setCellValue("G$cellNumber", $hasil_rupiah);
                $sheet->setCellValue("H$cellNumber", count($arrayTglCuti));
                $sheet->setCellValue("I$cellNumber", count($tglAlpha));
                $sheet->setCellValue("J$cellNumber", count($arrayTglSakit));
                $sheet->setCellValue("K$cellNumber", count($arrayTglIzin));
                $sheet->setCellValue("L$cellNumber", "$totalHari Hari");
                $sheet->setCellValue("M$cellNumber", "$totalJam Jam");
                $sheet->setCellValue("N$cellNumber", count($tglTidakHadir) ." Hari");
                $sheet->setCellValue("O$cellNumber", "$totalTerlambat");
                $sheet->setCellValue("P$cellNumber", "$hasilPotongan");
                $no_cell++;
                $cellNumber++;
            }
        }

        $styleResult = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];
        $highestRow = $sheet->getHighestRow();
        $sheet->getStyle('D15:P'.$highestRow)->applyFromArray($styleResult);
        $spreadsheet->getActiveSheet()->getStyle('D15:P'.$highestRow)->getAlignment()->setWrapText(true);
        // $spreadsheet->getActiveSheet()->getStyle("H10:M$highestRow")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('9ED2C6');
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $namaFile . '.xlsx"');
        return $writer->save("php://output");
    }

    public function importReport(Request $request)
    {
        $alphabetExcel = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG");

        // dd('public/storage/'.$request->oldExcel);
        if ($request->oldExcel) {
            $deleted_file = Storage::delete('public/'.$request->oldExcel);
        }
        $filetmp = $request->file('file_import')->store('post-excel', 'public');
        $filename = $request->file('file_import')->getClientOriginalName();
        // dd($filename);
        // $path = TTexcel::get();
        if ($filetmp) {
            $updateExcel = TTexcel::find($request->idoldExcel);
            $updateExcel->file_tmp=$filetmp;
            $updateExcel->file_name=$filename;
            $updateExcel->save();
            if ($updateExcel) {
                $ttexcel = TTexcel::where('file_name', $filename)->first();
            };
        };
        $path = public_path()."/storage/".$ttexcel->file_tmp;
        $reader = new ReaderXlsx(); 
        $spreadsheet = $reader->load($path);
        $sheet = $spreadsheet->getActiveSheet();

        $worksheetInfo = $reader->listWorksheetInfo($path);
        $totalRows = $worksheetInfo[0]['totalRows'];
        $totalColumn = $worksheetInfo[0]['totalColumns'];
        $lastAlpColumn = $alphabetExcel[$totalColumn];
        // dd($totalRows);
        for ($row=13; $row < $totalRows+1; $row++) { 
            $id[] = $sheet->getCell("D$row")->getValue();
        };
        
        $abjad = array();
        for ($i = 'F'; $i !== $lastAlpColumn; $i++){
            $abjad[]= $i; 
        };

        $noAbj = 0;
        $noAbs = 0;
        foreach ($abjad as $key => $value) {
            if ($noAbj == $key) {
                $dataRaw[]= $sheet->getCell($value."9")->getValue();
                // echo"<pre>";
                // print_r($dataRaw);
                //     for ($row=13; $row < $totalRows; $row++) { 
                //         $H[] = $sheet->getCell($value.$row)->getValue();
                //         $J[] = $sheet->getCell($abjad[$noAbj+1].$row)->getValue();
                //         $C[] = $sheet->getCell($abjad[$noAbj+2].$row)->getValue();
                //         $A[] = $sheet->getCell($abjad[$noAbj+3].$row)->getValue();
                //         $S[] = $sheet->getCell($abjad[$noAbj+4].$row)->getValue();
                //         $I[] = $sheet->getCell($abjad[$noAbj+5].$row)->getValue();
                //         // dd($abjad[$noAbj+1]);
                //         echo"<pre>";
                //         print_r($H);
                //         print_r($J);
                //         print_r($C);
                //         print_r($A);
                //         print_r($S);
                //         print_r($I);
                //         echo"</pre>";
                //     };
                
                // $data[$sheet->getCell($value."9")->getValue()] = 
                // [
                //     'H' => $H[$noAbs],
                //     'J' => $J[$noAbs],
                //     'C' => $C[$noAbs],
                //     'A' => $A[$noAbs],
                //     'S' => $S[$noAbs],
                //     'I' => $I[$noAbs]
                // ];

                $noAbs++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
            }
        }
        
        foreach ($id as $indeks => $val) {
            $dataRaaww[$val]=$dataRaw;
        }
        foreach ($dataRaaww as $k => $i) {
            foreach ($i as $p => $s) {
                dd($k);
            }
        }
        $noAbj = 0;
        $noAbs = 0;
        foreach ($abjad as $key => $value) {
            if ($noAbj == $key) {
                for ($row=13; $row < $totalRows; $row++) { 
                    $H[] = $sheet->getCell($value.$row)->getValue();
                    $J[] = $sheet->getCell($abjad[$noAbj+1].$row)->getValue();
                    $C[] = $sheet->getCell($abjad[$noAbj+2].$row)->getValue();
                    $A[] = $sheet->getCell($abjad[$noAbj+3].$row)->getValue();
                    $S[] = $sheet->getCell($abjad[$noAbj+4].$row)->getValue();
                    $I[] = $sheet->getCell($abjad[$noAbj+5].$row)->getValue();
                };
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
                $noAbj++;
            }
        }
        dd($H);
        die();
        // dd($id, $data);


    }

    public static function convertbulan($bulan)
    {
        switch ($bulan) {
            case '01':
            return $bulan = 'Januari';
            case '02':
            return $bulan = 'Februari';
            case '03':
            return $bulan = 'Maret';
            case '04':
            return $bulan = 'April';
            case '05':
            return $bulan = 'Mei';
            case '06':
            return $bulan = 'Juni';
            case '07':
            return $bulan = 'Juli';
            case '08':
            return $bulan = 'Agustus';
            case '09':
            return $bulan = 'September';
            case '10':
            return $bulan = 'Oktober';
            case '11':
            return $bulan = 'November';
            case '12':
            return $bulan = 'Desember';
            default:
            return $bulan = 'Bulan tidak valid';
        }
    }
    
    public static function convertbulaninggris($bulan)
    {
        switch ($bulan) {
            case 'Januari':
            return $bulan = 'January';
            case 'Februari':
            return $bulan = 'February';
            case 'Maret':
            return $bulan = 'March';
            case 'April':
            return $bulan = 'April';
            case 'Mei':
            return $bulan = 'May';
            case 'Juni':
            return $bulan = 'June';
            case 'Juli':
            return $bulan = 'July';
            case 'Agustus':
            return $bulan = 'August';
            case 'September':
            return $bulan = 'September';
            case 'Oktober':
            return $bulan = 'October';
            case 'November':
            return $bulan = 'November';
            case 'Desember':
            return $bulan = 'December';
            default:
            return $bulan = 'Bulan tidak valid';
        }
    }

    public static function getDataTotalAbsensi($tanggal)
    {
        $user = User::select('users.*')->where('role_id', 2)->get();
        foreach ($user as $index => $user_id) {       
            $TotalAbsensi = Total_absensi::select('tanggal', 'jmlh_hari', 'jmlh_jam', 'jmlh_sakit', 'jmlh_cuti', 'jmlh_izin', 'jmlh_alpha')
            ->where('tanggal', $tanggal)
            ->get();            
            $dataRaw[$user_id->id] = $TotalAbsensi;
        }

        return $dataRaw;
    }

    public static function sumDaysOfMonth($days = 0, $month, $type) {
        
        $startMonth = Carbon::now()->month($month)->day(1)->format("Y-m-d");
        $endMonth = Carbon::now()->month($month)->endOfMonth()->format("Y-m-d");
        $start = new DateTime($startMonth);
        $end = new DateTime($endMonth);

        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');

        $interval = $end->diff($start);

        // total days
        $days = $interval->days;

        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        
        // best stored as array, so you can add more than one
        $liburnas = TMliburnas::select('tanggal')->whereMonth('tanggal', '=', $month)->pluck('tanggal')->toArray();
        // dd($liburnas);
        foreach($period as $dt) {
            $curr = $dt->format('D');

            // substract if Saturday or Sunday
        
            if ($type == 2) {
                if ($curr == 'Sun') {
                    $days--;
                }elseif (in_array($dt->format('Y-m-d'), $liburnas)) {
                    $days--;
                }
            }else {
                if ($curr == 'Sat' || $curr == 'Sun') {
                    $days--;
                }elseif (in_array($dt->format('Y-m-d'), $liburnas)) {
                    $days--;
                }
            }
        }
        
        return $days;
    }
    
    public static function sumDaysBetweenDate($days = 0, $startDate, $endDate, $type) {
        // dd($type);
        $start = new DateTime(date('Y-m-d', strtotime($startDate)));
        $end = $endDate;
        $startMonth = date('m', strtotime($startDate));
        $endMonth = date('m', strtotime($startDate));
        // dd($startDate);

        // otherwise the  end date is excluded (bug?)
        // $end->modify('+1 day');

        $interval = $end->diff($start);

        // total days
        $days = $interval->days;
        // dd($days);
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        
        // best stored as array, so you can add more than one
        $liburnas = TMliburnas::select('tanggal')->whereBetween('tanggal', [$start, $end])->pluck('tanggal')->toArray();
        // dd($liburnas);
        foreach($period as $dt) {
            $curr = $dt->format('D');

            // substract if Saturday or Sunday
        
            if ($type == 2) {
                if ($curr == 'Sun') {
                    $days--;
                }elseif (in_array($dt->format('Y-m-d'), $liburnas)) {
                    $days--;
                }
            }else {
                if ($curr == 'Sat' || $curr == 'Sun') {
                    $days--;
                }elseif (in_array($dt->format('Y-m-d'), $liburnas)) {
                    $days--;
                }
            }
        }
        
        return $days;
    }
}
