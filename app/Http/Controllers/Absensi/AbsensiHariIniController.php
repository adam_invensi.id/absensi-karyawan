<?php

namespace App\Http\Controllers\Absensi;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Absensi;
use App\Models\TMabsen;
use App\Models\TMjamkerja;
use App\Models\TMlokasi;
use App\Models\Total_absensi;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use DOMDocument;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AbsensiHariIniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('users.*', 'tm_biodata.id as biodata_id', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.departemen_id', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->where('users.role_id', 2)
        ->get();
        
        $bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');

        return view('absensi.index', compact('users', 'bulan'));
    }

    public function absenHariIni(Request $request)
    {
        // dd($request, date('H:i'));
        // $ip = '182.253.127.41'; /* Static IP address */
        // $currentUserInfo = Location::get($ip);
        // dd($currentUserInfo->regionName, $currentUserInfo->cityName, $currentUserInfo->zipCode, $currentUserInfo->latitude, $currentUserInfo->longitude );
        if ($request->absen == 4) {
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'lokasi' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'lokasi.required' => 'Harap isi bidang Lokasi'
                ]);
            }
        }elseif ($request->absen == 6) {
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'tanggal_selesai_cuti' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
                'keterangan' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'tanggal_selesai_cuti.required' => 'Harap isi bidang Tanggal Selesai Cuti',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
                'keterangan.required' => 'Harap isi bidang Keterangan',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'tanggal_selesai_cuti' => 'required',
                    'lokasi' => 'required',
                    'keterangan' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'tanggal_selesai_cuti.required' => 'Harap isi bidang Tanggal Selesai Cuti',
                    'lokasi.required' => 'Harap isi bidang Lokasi',
                    'keterangan.required' => 'Harap isi bidang Keterangan'
                ]);
            }
        }else{
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'jam' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'jam.required' => 'Harap isi bidang Jam',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'jam' => 'required',
                    'lokasi' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'jam.required' => 'Harap isi bidang Jam',
                    'lokasi.required' => 'Harap isi bidang Lokasi'
                ]);
            }
        }
        $user = User::where('email', $request['email'])->first();
        $total_absensi = Total_absensi::where('user_id', $user->id)->where('tanggal', date('Y-m'))->first();
        $date = date('Y-m-d');
        $absensi = Absensi::where('user_id', '=', $user->id)->where('tanggal_absen', '=', $date)->first();
        // dd($absensi);
        if($absensi == null){
            if ($user) {
                if($request['absen'] == 1){
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'jam_masuk' => ($request['absen'] == 1)? date('H:i') : NULL,
                        'jam_pulang' => ($request['absen'] == 2)? date('H:i') : NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input);
                }
                if($request['absen'] == 3){
                    
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_izin' => 1,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_izin' => $total_absensi->jmlh_izin+1,
                        ]);
                    }
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input);
                }
                if($request['absen'] == 6){
                    
                    $tanggal_mulai_cuti = new DateTime($request->tanggal);
                    $tanggal_selesai_cuti = new DateTime($request->tanggal_selesai_cuti);
                    $interval = $tanggal_mulai_cuti->diff($tanggal_selesai_cuti);
                    $days = $interval->format('%a');
                    // untuk mengurangi cuti
                    // $pos_diff = $earlier->diff($later)->format("%r%a"); //3
                    // $neg_diff = $later->diff($earlier)->format("%r%a"); //-3
                    
                    if ($total_absensi == null) {
                        $insert_cuti = Total_absensi::create([
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_cuti' => $days,
                        ]);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_cuti' => $total_absensi->jmlh_cuti+$days,
                        ]);
                    }
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'jam_pulang' => NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    Absensi::create($input);
                    
                    $input2 = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => $request['tanggal_selesai_cuti'],
                        'jam_pulang' => NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input2);
                }
                if ($request['absen'] == 4) {
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_sakit' => 1,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_sakit' => $total_absensi->jmlh_sakit+1,
                        ]);
                    }
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'jam_masuk' => NULL,
                        'jam_pulang' => NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input);
                }
                if ($request['absen'] == 5) {
                    $total_hariini = $this->getDurationAttribute($absensi->jam_masuk, date('H:i'));
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_hari' => 1,
                            'jmlh_jam' => (date('H:i') > "13:00") ? $total_hariini-1 : $total_hariini,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                        'jmlh_hari' => $total_absensi->jmlh_hari+1,
                        'jmlh_jam' => (date('H:i') > "13:00") ? $total_absensi->jmlh_jam+($total_hariini-1) : $total_absensi->jmlh_jam+$total_hariini,
                        ]);
                    }
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'jam_pulang' => ($request['absen'] == 5)? date('H:i') : NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input);
                }
            }else{
                return redirect()->back()->with('failed', 'Email Tidak ada!');
            }
        }else{
            if ($absensi->jam_masuk != null) {
                $total_hariini = $this->getDurationAttribute($absensi->jam_masuk, date('H:i'));
            }
            
            if ($user) {
                if ($request['absen'] == 1) {
                    $absen = null;
                }
                if ($request['absen'] == 2) {
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_hari' => 1,
                            'jmlh_jam' => $total_hariini-1,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_hari' => $total_absensi->jmlh_hari+1,
                            'jmlh_jam' => $total_absensi->jmlh_jam+($total_hariini-1),
                        ]);
                    }
                    $absen = DB::table('absensi')
                    ->where('tanggal_absen', $date)
                    ->where('user_id', $user->id)
                    ->update([
                        'absen_id' => $request['absen'],
                        'jam_pulang' => date('H:i'),
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    ]);
                }
                if($request['absen'] == 3){
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_izin' => 1,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_izin' => $total_absensi->jmlh_izin+1,
                        ]);
                    }
                    $absen = DB::table('absensi')
                    ->where('tanggal_absen', $date)
                    ->where('user_id', $user->id)
                    ->update([
                        'absen_id' => $request['absen'],
                        'jam_pulang' => date('H:i'),
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    ]);
                }
                if ($request['absen'] == 5) {
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_hari' => 1,
                            'jmlh_jam' => (date('H:i') > "13:00") ? $total_hariini-1 : $total_hariini,
                        );
                        Total_absensi::create($input);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                        'jmlh_hari' => $total_absensi->jmlh_hari+1,
                        'jmlh_jam' => (date('H:i') > "13:00") ? $total_absensi->jmlh_jam+($total_hariini-1) : $total_absensi->jmlh_jam+$total_hariini,
                        ]);
                    }
                    
                    $absen = DB::table('absensi')
                    ->where('tanggal_absen', $date)
                    ->where('user_id', $user->id)
                    ->update([
                        'absen_id' => $request['absen'],
                        'jam_pulang' => date('H:i'),
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    ]);
                }
                if($request['absen'] == 6){
                    
                    $tanggal_mulai_cuti = new DateTime($request->tanggal);
                    $tanggal_selesai_cuti = new DateTime($request->tanggal_selesai_cuti);
                    $interval = $tanggal_mulai_cuti->diff($tanggal_selesai_cuti);
                    $days = $interval->format('%a');
                    // untuk mengurangi cuti
                    // $pos_diff = $earlier->diff($later)->format("%r%a"); //3
                    // $neg_diff = $later->diff($earlier)->format("%r%a"); //-3
                    
                    if ($total_absensi == null) {
                        $insert_cuti = Total_absensi::create([
                            'user_id' => $user->id,
                            'tanggal' => date('Y-m'),
                            'jmlh_cuti' => $days,
                        ]);
                    }else{
                        $total_update = DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_cuti' => $total_absensi->jmlh_cuti+$days,
                        ]);
                    }
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => date('Y-m-d'),
                        'jam_pulang' => NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    Absensi::create($input);
                    
                    $input2 = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => $request['tanggal_selesai_cuti'],
                        'jam_pulang' => NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input2);
                }
            }else{
                return redirect()->back()->with('failed', 'Email Tidak ada!');
            }
        }
        
        if ($absen) {
            return redirect()->back()->with('success', 'Absen Hari ini sukses! Semoga harimu menyenangkan! :D');
        }else {
            return redirect()->back()->with('failed', 'Absen hari ini gagal.');
        }
        return redirect()->back()->with('failed', 'Absen hari ini gagal.');
    }

    public function show($id)
    {
        $users = User::select('users.*', 'tm_biodata.id as biodata_id', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.departemen_id', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->where('users.id', $id)
        ->first();
        
        $karyawan = Absensi::select('absensi.*', 'users.nama', 'users.username', 'users.email', 'users.role_id', 'users.status','tm_absen.absen', 'tm_lokasi.lokasi', 'tm_biodata.no_telepon', 'tm_biodata.jabatan_id', 'tm_biodata.alamat', 'tm_jabatan.departemen_id', 'tm_departemen.departemen')
        ->join('users', 'absensi.user_id', '=', 'users.id')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->join('tm_absen', 'absensi.absen_id', '=', 'tm_absen.id')
        ->join('tm_lokasi', 'absensi.lokasi_id', '=', 'tm_lokasi.id')
        ->where('absensi.user_id', $id)
        ->orderBy('absensi.tanggal_absen', 'DESC')
        ->get();
        $bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');

        // dd($karyawan);
        return view('absensi.show', compact('users' ,'karyawan', 'bulan'));
    }

    public function show_old(Request $request)
    {
        // dd($request, date('H:i'));
        if ($request->absen == 4) {
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'lokasi' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'lokasi.required' => 'Harap isi bidang Lokasi'
                ]);
            }
        }elseif ($request->absen == 6) {
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'tanggal_selesai_cuti' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
                'keterangan' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'tanggal_selesai_cuti.required' => 'Harap isi bidang Tanggal Selesai Cuti',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
                'keterangan.required' => 'Harap isi bidang Keterangan',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'tanggal_selesai_cuti' => 'required',
                    'lokasi' => 'required',
                    'keterangan' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'tanggal_selesai_cuti.required' => 'Harap isi bidang Tanggal Selesai Cuti',
                    'lokasi.required' => 'Harap isi bidang Lokasi',
                    'keterangan.required' => 'Harap isi bidang Keterangan'
                ]);
            }
        }else{
            if ($request->lokasi == 4) {
                $validated = $request->validate([
                'email' => 'required|email',
                'absen' => 'required',
                'tanggal' => 'required',
                'jam' => 'required',
                'lokasi' => 'required',
                'lainnya' => 'required',
            ],[
                'email.required' => 'Harap isi bidang Email',
                'email.email' => 'Email tidak valid',
                'absen.required' => 'Harap isi bidang Absen',
                'tanggal.required' => 'Harap isi bidang Tanggal',
                'jam.required' => 'Harap isi bidang Jam',
                'lokasi.required' => 'Harap isi bidang Lokasi',
                'lainnya.required' => 'Harap isi bidang Lainnya',
            ]);
            }else{
                $validated = $request->validate([
                    'email' => 'required|email',
                    'absen' => 'required',
                    'tanggal' => 'required',
                    'jam' => 'required',
                    'lokasi' => 'required',
                ],[
                    'email.required' => 'Harap isi bidang Email',
                    'email.email' => 'Email tidak valid',
                    'absen.required' => 'Harap isi bidang Absen',
                    'tanggal.required' => 'Harap isi bidang Tanggal',
                    'jam.required' => 'Harap isi bidang Jam',
                    'lokasi.required' => 'Harap isi bidang Lokasi'
                ]);
            }
        }
        $user = User::where('email', $request['email'])->first();
        // dd($user);
        $total_absensi = Total_absensi::where('user_id', $user->id)->first();
        $date = date('Y-m-d');
        $absensi = Absensi::where('user_id', '=', $user->id)->where('tanggal_absen', '=', $request['tanggal'])->first();

        if($absensi == null){
            // die('exs');
            if ($user) {
                if($request['absen'] == 1){
                    $input = array(
                        'user_id' => $user->id,
                        'absen_id' => $request['absen'],
                        'lokasi_id' => $request['lokasi'],
                        'tanggal_absen' => $request['tanggal'],
                        'jam_masuk' => ($request['absen'] == 1)? $request['jam'] : NULL,
                        'jam_pulang' => ($request['absen'] == 2)? $request['jam'] : NULL,
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    );
                    $absen = Absensi::create($input);
                }
            }else{
                return redirect()->back()->with('failed', 'Email Tidak ada!');
            }
        }else{
            if ($absensi->jam_masuk != null) {
                $total_hariini = $this->getDurationAttribute($absensi->jam_masuk, $request['jam']);
                // dd($total_hariini);
            }
            
            if ($user) {
                if ($request['absen'] == 1) {
                    $absen = null;
                }
                if ($request['absen'] == 2) {
                    if ($total_absensi == null) {
                        $input = array(
                            'user_id' => $user->id,
                            'tanggal' => $request['tanggal'],
                            'jmlh_hari' => 1,
                            'jmlh_jam' => $total_hariini-1,
                        );
                        Total_absensi::create($input);
                    }else{
                        DB::table('total_absensi')
                        ->where('user_id', $user->id)
                        ->update([
                            'jmlh_hari' => $total_absensi->jmlh_hari+1,
                            'jmlh_jam' => $total_absensi->jmlh_jam+($total_hariini-1),
                        ]);
                        // dd($total_update);
                    }
                    // die('eai');
                    $absen = DB::table('absensi')
                    ->where('tanggal_absen', $request['tanggal'])
                    ->where('user_id', $user->id)
                    ->update([
                        'absen_id' => $request['absen'],
                        'jam_pulang' => $request['jam'],
                        'lainnya' => ($request->has('lainnya'))? $request['lainnya'] : '',
                        'keterangan' => ($request->has('keterangan'))? $request['keterangan'] : ''
                    ]);
                }
            }else{
                return redirect()->back()->with('failed', 'Email Tidak ada!');
            }
        }
        
        if ($absen) {
            return redirect()->back()->with('success', 'Absen Hari ini sukses! Semoga harimu menyenangkan! :D');
        }else {
            return redirect()->back()->with('failed', 'Absen hari ini gagal.');
        }
        return redirect()->back()->with('failed', 'Absen hari ini gagal.');
    }

    public function filter_by_month(Request $request)
    {
        $id = $request->id;
        $filter = array();
        if ($request->has('bulan')) {
            $bulan_selected = $request->bulan;
            if ($request->has('tahun')) {
                $tahun = $request->tahun;
                $karyawan = Absensi::select('absensi.*', 'users.nama', 'users.username', 'users.email', 'users.role_id', 'users.status','tm_absen.absen', 'tm_lokasi.lokasi', 'tm_biodata.no_telepon', 'tm_biodata.alamat', 'tm_jabatan.departemen_id', 'tm_departemen.departemen')
                ->join('users', 'absensi.user_id', '=', 'users.id')
                ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
                ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
                ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
                ->join('tm_absen', 'absensi.absen_id', '=', 'tm_absen.id')
                ->join('tm_lokasi', 'absensi.lokasi_id', '=', 'tm_lokasi.id')
                ->where('absensi.user_id', $id)
                ->whereYear('absensi.tanggal_absen' ,'=' ,$tahun)
                ->whereMonth('absensi.tanggal_absen' ,'=' ,$bulan_selected)
                ->get();
            }
        }

        $users = User::select('users.*', 'tm_biodata.id as biodata_id', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_jabatan.departemen_id', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
        ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
        ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
        ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
        ->where('users.id', $id)
        ->first();
        
        $bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');

        return view('absensi.show', compact('users' ,'karyawan', 'bulan', 'bulan_selected', 'tahun'));
    }

    public function exportExcelAll(Request $request)
    {
        $bulan = $this->convertbulan($request->bulan);
        $tahun_bulan = $request->tahun.'-'.$request->bulan;
        $dataAbsen = $this->getDataAbsensi();
        $namaFile = "Absensi Karyawan $bulan $request->tahun";

		$spreadsheet = new Spreadsheet();
        $html = new \PhpOffice\PhpSpreadsheet\Helper\Html();
		$sheet = $spreadsheet->getActiveSheet();
        $styleArrayTitleContent = [
			'font' => [
				'bold' => true,
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
		];
        
        
        $abjad = array();
        $limitationcellPerMonth = $this->limitationcellPerMonth($this->days_in_month($request->bulan, $request->tahun));
        for ($i = 'M'; $i !== $limitationcellPerMonth; $i++){
            $abjad= $i; 
        }
		$sheet->getStyle("D10:".$abjad."13")->applyFromArray($styleArrayTitleContent);
		
        $sheet->mergeCells("M10:". $abjad ."10")->setCellValue('M10', 'Hari & Tanggal');
        
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(6);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(6);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(6);
        $abjad = array();
        $limitationcellPerMonth = $this->limitationcellPerMonth($this->days_in_month($request->bulan, $request->tahun));
        for ($i = 'M'; $i !== $limitationcellPerMonth; $i++){
            $abjad[]= $i; 
        }
        foreach ($abjad as $key => $value) {
            $spreadsheet->getActiveSheet()->getColumnDimension($value)->setWidth(6);
        }

        $sheet->setCellValue('D6', 'PT. Invensi Digital Nusantara');
        $sheet->setCellValue('D7', 'Absensi Karyawan');
        $sheet->setCellValue('D8', "Bulan $bulan $request->tahun");
        $sheet->mergeCells('D10:D13')->setCellValue('D10', 'No.');
        $sheet->mergeCells('E10:E13')->setCellValue('E10', 'Nama Karyawan');
        $sheet->mergeCells('F10:F13')->setCellValue('F10', 'N P W P');
        $sheet->mergeCells('G10:H11')->setCellValue('G10', 'Jumlah Hadir');
        $sheet->mergeCells('G12:G13')->setCellValue('G12', 'Hari');
        $sheet->mergeCells('H12:H13')->setCellValue('H12', 'Jam');
        $sheet->mergeCells('I10:I13')->setCellValue('I10', 'Jmlh Sakit');
        $sheet->mergeCells('J10:J13')->setCellValue('J10', 'Jmlh Cuti');
        $sheet->mergeCells('K10:K13')->setCellValue('K10', 'Jmlh Izin');
        $sheet->mergeCells('L10:L13')->setCellValue('L10', 'Jmlh Alpha');        
        
        
        $abjad = array();
        $limitationcellPerMonth = $this->limitationcellPerMonth($this->days_in_month($request->bulan, $request->tahun));
        for ($i = 'M'; $i !== $limitationcellPerMonth; $i++){
            $abjad[]= $i; 
        }
        
        $no_cell=1;
        $cellNumber = 14;
        $cellNumberforID = 14;
        foreach ($dataAbsen as $user_id => $absensi) {
            $user = User::select('users.*', 'tm_biodata.npwp', 'total_absensi.jmlh_hari', 'total_absensi.jmlh_jam', 'total_absensi.jmlh_sakit', 'total_absensi.jmlh_cuti', 'total_absensi.jmlh_izin', 'total_absensi.jmlh_alpha')->join('total_absensi', 'users.id', '=', 'total_absensi.user_id')->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')->where('users.id', $user_id)->where('role_id', 2)->get();
            foreach ($user as $key => $value) {
                $sheet->setCellValue("D$cellNumber", $no_cell);
                $sheet->setCellValue("E$cellNumber", $value->nama);
                $sheet->setCellValue("F$cellNumber", $value->npwp);
                foreach ($absensi as $indeks => $abs) {
                    $no = 0;
                    $date = date('F Y', strtotime($tahun_bulan));
                    while (strtotime($date) <= strtotime(date('Y-m', strtotime($tahun_bulan)) . '-' . date('t', strtotime($date)))) {
                        $day_num = date('j', strtotime($date));//Day number
                        $month = date('m', strtotime($date));//Month number
                        $year = date('Y', strtotime($date));//Year
                        $day_name = $this->hariIndo(date('l', strtotime($date)));//Day name
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));//Adds 1 day onto current date
                        
                        if ($year == date('Y', strtotime($abs->tanggal_absen))) {
                            if ($month == date('m', strtotime($abs->tanggal_absen))) {
                                if ($day_num == date('d', strtotime($abs->tanggal_absen))) {    
                                    $sheet->setCellValue("G$cellNumber", $value->jmlh_hari);    
                                    $sheet->setCellValue("H$cellNumber", $value->jmlh_jam);    
                                    $sheet->setCellValue("I$cellNumber", $value->jmlh_sakit);    
                                    $sheet->setCellValue("J$cellNumber", $value->jmlh_cuti);    
                                    $sheet->setCellValue("K$cellNumber", $value->jmlh_izin);    
                                    $sheet->setCellValue("L$cellNumber", $value->jmlh_alpha);
                                    if ($abs->absen_id == 2) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, ($abs->jam_masuk != null) ?\Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i') : '-');
                                        $sheet->setCellValue($abjad[$no+1]. $cellNumber, ($abs->jam_pulang != null) ?\Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i') : '-');
                                    }elseif ($abs->absen_id == 1) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, ($abs->jam_masuk != null) ? \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i') : '-');
                                    }elseif ($abs->absen_id == 3) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, ($abs->jam_masuk != null) ?\Carbon\Carbon::createFromFormat('H:i:s', $abs->jam_masuk)->format('h:i') : '-');
                                        $sheet->setCellValue($abjad[$no+1]. $cellNumber, ($abs->jam_pulang != null) ? \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i')." (Izin)" : ' (Izin)');
                                    }elseif ($abs->absen_id == 4) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, "S");
                                        $sheet->setCellValue($abjad[$no+1]. $cellNumber, "S");
                                    }elseif ($abs->absen_id == 5) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, ($abs->jam_masuk != null) ? \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i') : '-');
                                        $sheet->setCellValue($abjad[$no+1]. $cellNumber, ($abs->jam_pulang != null) ? \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i')." (Izin)" : '-');
                                    }elseif ($abs->absen_id == 6) {
                                        $sheet->setCellValue($abjad[$no]. $cellNumber, "C");
                                        $sheet->setCellValue($abjad[$no+1]. $cellNumber, "C");
                                    }
                                }
                            }
                        }
                        $no++;
                        $no++;
                    }
                }
            }
            $cellNumber++;
            $no_cell++;
        }

        $no = 0;
        $date = date('F Y', strtotime($tahun_bulan));
        while (strtotime($date) <= strtotime(date('Y-m', strtotime($tahun_bulan)) . '-' . date('t', strtotime($date)))) {
            $day_num = date('j', strtotime($date));//Day number
            $day_name = $this->hariIndo(date('l', strtotime($date)));//Day name
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));//Adds 1 day onto current date
            $highestRow = $sheet->getHighestRow();
            if($day_name == "Sab"){
                if (isset($abjad[$no])) {
                $spreadsheet->getActiveSheet()->getStyle($abjad[$no]."11:" . $abjad[$no+1] . "13")->getFont()->getColor()->setARGB('bf28d3');
                $spreadsheet->getActiveSheet()->getStyle($abjad[$no]."11:" . $abjad[$no+1] . "$highestRow")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('c2ffc2');
                }
            }elseif ($day_name == "Ming") {
                if (isset($abjad[$no])) {
                $spreadsheet->getActiveSheet()->getStyle($abjad[$no]."11:" . $abjad[$no+1] . "13")->getFont()->getColor()->setARGB('c43c4a');
                $spreadsheet->getActiveSheet()->getStyle($abjad[$no]."11:" . $abjad[$no+1] . "$highestRow")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('c2ffc2');
                }
            }
            // dd($day_name);
            if (isset($abjad[$no])) {
                $sheet->mergeCells($abjad[$no]."11:".$abjad[$no+1]."11")->setCellValue($abjad[$no]."11", isset($day_name) ? $day_name : '');
                $sheet->mergeCells($abjad[$no]."12:".$abjad[$no+1]."12")->setCellValue($abjad[$no]."12", $day_num);
                $sheet->setCellValue($abjad[$no]."13", "M");
                $sheet->setCellValue($abjad[$no+1]."13", "P");
            }

            $no++;
            $no++;
        }

        $styleArrayContent = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
		];
        $styleResult = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				],
			],
		];

        $styleVerticalResult = [
			'borders' => [
				'vertical' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				]
			],
		];

        $styleBottomResult = [
			'borders' => [
				'bottom' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				]
			],
		];
        
        $abjad = array();
        $limitationcellPerMonth = $this->limitationcellPerMonth($this->days_in_month($request->bulan, $request->tahun));
        for ($i = 'M'; $i !== $limitationcellPerMonth; $i++){
            $abjad= $i; 
        }
        $highestRow = $sheet->getHighestRow();
        $sheet->getStyle("D14:".$abjad."$highestRow")->applyFromArray($styleArrayContent);
		$sheet->getStyle("D10:F$highestRow")->applyFromArray($styleResult);
		$sheet->getStyle("G10:L13")->applyFromArray($styleResult);
		$sheet->getStyle("G14:L$highestRow")->applyFromArray($styleVerticalResult);
		$sheet->getStyle("G20:L$highestRow")->applyFromArray($styleBottomResult);
		$sheet->getStyle("M10:$abjad"."$highestRow")->applyFromArray($styleResult);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $namaFile . '.xlsx"');
        return $writer->save("php://output");
    }
    
    public function exportExcelUser(Request $request)
    {
        // dd($request->tahun);
        $bulan = $this->convertbulan($request->bulan);
        if($request->bulan == "01"){
            $start_date = (intval($request->tahun)-1) . "-12-26";
        }else {
            $start_date = $request->tahun . "-". (intval($request->bulan)-1) . "-26";
        }
        $end_date = $request->tahun . "-".$request->bulan . "-25";
        
        $user = User::select('users.*', 'tm_biodata.npwp')->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')->where('users.id', $request->id)->where('role_id', 2)->first();
        $tm_absen = TMabsen::select('*')->get();
        
        $absensi = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang', 'keterangan')
        ->where('user_id', $user->id)
        ->whereRaw("tanggal_absen >= '". $start_date ."' AND tanggal_absen <= '". $end_date . "'")
        ->get();
        // dd($absensi);
        $namaFile = "Absensi Karyawan $bulan $request->tahun";
        $spreadsheet = new Spreadsheet();
        $html = new \PhpOffice\PhpSpreadsheet\Helper\Html();
		$sheet = $spreadsheet->getActiveSheet();
        $styleArrayTitle = [
			'font' => [
                'bold' => true,
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
		];
        
        $styleArrayContent = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
		];
		$sheet->getStyle("D11:"."J11")->applyFromArray($styleArrayTitle);
		$sheet->getStyle("D9:"."J".$sheet->getHighestRow())->applyFromArray($styleArrayContent);
        $sheet->getStyle("D10:"."J".$sheet->getHighestRow())->getAlignment()->setWrapText(true);
        
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(7);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        
        $sheet->mergeCells('D9:J9')->setCellValue('D9', 'PT. Invensi Digital Nusantara');
        $sheet->mergeCells('H10:J10')->setCellValue('H10', 'Bulan ' . $this->convertbulan($request->bulan) . " " . $request->tahun);
        $sheet->mergeCells('D10:F10')->setCellValue('D10', $user->nama);
        
        $sheet->setCellValue('D11', 'No.');
        $sheet->setCellValue('E11', 'Hari');
        $sheet->setCellValue('F11', 'Tanggal');
        $sheet->setCellValue('G11', 'HK');
        $sheet->setCellValue('H11', 'Jam Masuk');
        $sheet->setCellValue('I11', 'Jam Pulang');
        $sheet->setCellValue('J11', 'Keterangan');

        $no_cell=1;
        $cellNumber = 12;
        if ($absensi->isEmpty() == false) {
            $hadir = 0;
            foreach ($absensi as $indeks => $abs) {
                // dd(date("m", strtotime($abs->tanggal_absen)), $request->bulan);
                $sheet->setCellValue("D$cellNumber", $no_cell);
                foreach ($tm_absen as $indek => $absen) {
                    if ($abs->absen_id == $absen->id) {
                        $hari = null;
                        switch (date("w", strtotime($abs->tanggal_absen))) {
                            case '0':
                            $hari = 'Minggu';
                            break;
                            case '1':
                            $hari = 'Senin';
                            break;
                            case '2':
                            $hari = 'Selasa';
                            break;
                            case '3':
                            $hari = 'Rabu';
                            break;
                            case '4':
                            $hari = 'Kamis';
                            break;
                            case '5':
                            $hari = 'Jumat';
                            break;
                            case '6':
                            $hari = 'Sabtu';
                            break;
                            default:
                            $hari = 'hari tidak valid';
                            break;
                        }
                        $sheet->setCellValue("E$cellNumber", $hari);
                        
                        if ($absen->id == 2) {
                            $hadir++;
                            $sheet->setCellValue("G$cellNumber", 1);
                            $sheet->setCellValue("J$cellNumber", "Hadir ". $abs->keterangan);
                        }else{
                            $sheet->setCellValue("G$cellNumber", 0);
                            $sheet->setCellValue("J$cellNumber", $absen->absen . $abs->keterangan);
                        }
                        $sheet->setCellValue("F$cellNumber", $abs->tanggal_absen);
                        if ($abs->absen_id == 2) {
                            $sheet->setCellValue("H$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i'));
                            $sheet->setCellValue("I$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i'));
                        }elseif ($abs->absen_id == 1) {
                            $sheet->setCellValue("H$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i'));
                        }elseif ($abs->absen_id == 3) {
                            if ($abs->jam_masuk != null) {
                                $sheet->setCellValue("H$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i'));
                            }
                            if ($abs->jam_pulang != null) {
                                $sheet->setCellValue("I$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i')." (Izin)");
                            }
                        }elseif ($abs->absen_id == 4) {
                            $sheet->setCellValue("H$cellNumber", "S");
                            $sheet->setCellValue("J$cellNumber", "S");
                        }elseif ($abs->absen_id == 5) {
                            $sheet->setCellValue("H$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_masuk)->format('h:i'));
                            $sheet->setCellValue("I$cellNumber", \Carbon\Carbon::createFromFormat('H:i:s',$abs->jam_pulang)->format('h:i')." (Izin)");
                        }elseif ($abs->absen_id == 6) {
                            $sheet->setCellValue("H$cellNumber", "C");
                            $sheet->setCellValue("J$cellNumber", "C");
                        }

                    }
                }
                $no_cell++;
                $cellNumber++;
            }
        }else {
            $sheet->mergeCells('D12:J12')->setCellValue('D12', 'Tidak Ada Data.');
        }
        // dd($sheet->getHighestRow());
        $sheet->mergeCells('D'.($sheet->getHighestRow()+1).':F'.($sheet->getHighestRow()+1))->setCellValue('D'.$sheet->getHighestRow(), "Total Hari Kerja");
        $sheet->setCellValue('G'.$sheet->getHighestRow(), $hadir);
        $styleResult = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				],
			],'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
		];
        
		$sheet->getStyle('D11:'. "J" . $sheet->getHighestRow())->applyFromArray($styleResult);
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $namaFile . '.xlsx"');
        return $writer->save("php://output");
    }

    public static function hariIndo ($hariInggris) {
        switch ($hariInggris) {
            case 'Sunday':
            return 'Ming';
            case 'Monday':
            return 'Sen';
            case 'Tuesday':
            return 'Sel';
            case 'Wednesday':
            return 'Rab';
            case 'Thursday':
            return 'Kam';
            case 'Friday':
            return 'Jum';
            case 'Saturday':
            return 'Sab';
            default:
            return 'hari tidak valid';
        }
    }
    
    public static function limitationcellPerMonth ($month) {
        switch ($month) {
            case '31':
            return 'BW';
            case '30':
            return 'BU';
            case '28':
            return 'BQ';
            case '29':
            return 'BS';
            default:
            return 'Jumlah hari tidak valid';
        }
    }

    public static function days_in_month($month, $year)
    {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    public static function getDataAbsensi()
    {
        $user = User::select('users.*', 'tm_biodata.npwp')->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')->where('role_id', 2)->get();
        foreach ($user as $index => $user_id) {       
            $absensi = Absensi::select('tanggal_absen', 'absen_id', 'jam_masuk', 'jam_pulang')
            ->where('user_id', $user_id->id)
            ->groupBy('tanggal_absen')
            ->groupBy('absen_id')
            ->groupBy('jam_masuk')
            ->groupBy('jam_pulang')
            ->get();            
            $dataRaw[$user_id->id] = $absensi;
        }

        return $dataRaw;
    }

    public static function convertbulan($bulan)
    {
        switch ($bulan) {
            case '01':
            return $bulan = 'Januari';
            case '02':
            return $bulan = 'Februari';
            case '03':
            return $bulan = 'Maret';
            case '04':
            return $bulan = 'April';
            case '05':
            return $bulan = 'Mei';
            case '06':
            return $bulan = 'Juni';
            case '07':
            return $bulan = 'Juli';
            case '08':
            return $bulan = 'Agustus';
            case '09':
            return $bulan = 'September';
            case '10':
            return $bulan = 'Oktober';
            case '11':
            return $bulan = 'November';
            case '12':
            return $bulan = 'Desember';
            default:
            return $bulan = 'Bulan tidak valid';
        }
    }

    public static function getDurationAttribute($mulai, $selesai)
    {
        $in = Carbon::createFromFormat('H:i:s',$mulai);
        $out =  Carbon::createFromFormat('H:i',$selesai)->format('H:i:s');
        // dd($in, $out);
        
        return $in->diffAsCarbonInterval($out)->hours;
    }

    
    public function rv()
    {
        $absen = TMabsen::get();
        $date = date("Y-m-d");
        $absensi_pulang = Absensi::where('absen_id', '=', 2)->where('user_id', '=', auth()->user()->id)->where('tanggal_absen', '=', $date)->first();
        $absensi_masuk = Absensi::where('absen_id', '=', 1)->where('user_id', '=', auth()->user()->id)->where('tanggal_absen', '=', $date)->first();
        $lokasi = TMlokasi::get();
        $waktu = TMjamkerja::get();
        return view('absensi.show_old', compact('absen', 'lokasi', 'waktu', 'absensi_pulang', 'absensi_masuk'));
    }

}
