<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMlokasi extends Model
{
    protected $table = 'tm_lokasi';

    protected $fillable = [
        'lokasi'
    ];
}
