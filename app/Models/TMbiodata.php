<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMbiodata extends Model
{
    protected $table = 'tm_biodata';

    protected $fillable = [
        'user_id',
        'jabatan_id',
        'upah_id',
        'nik',
        'npwp',
        'tmk',
        'no_telepon',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'alamat',
        'agama',
        'status_perkawinan',
    ];
}
