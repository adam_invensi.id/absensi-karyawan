<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensi';

    protected $fillable = [
        'user_id', 'absen_id', 'lokasi_id', 'tanggal_absen', 'jam_masuk', 'jam_pulang', 'keterangan', 'lainnya', 'file'
    ];
}
