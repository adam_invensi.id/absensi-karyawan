<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TTexcel extends Model
{
    protected $table = 'tt_excel';

    protected $fillable = [
        'file_tmp',
        'file_name'
    ];
}
