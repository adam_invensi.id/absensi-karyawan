<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMliburnas extends Model
{
    protected $table = 'tm_liburnas';

    protected $fillable = [
        'tanggal',
        'keterangan'
    ];
}
