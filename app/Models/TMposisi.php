<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMposisi extends Model
{
    protected $table = 'tm_posisi';

    protected $fillable = [
        'posisi'
    ];
}
