<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMupah extends Model
{
    protected $table = 'tm_upah';

    protected $fillable = [
        'upah'
    ];
}
