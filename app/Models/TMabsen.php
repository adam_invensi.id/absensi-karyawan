<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMabsen extends Model
{
    protected $table = 'tm_absen';

    protected $fillable = [
        'absen'
    ];
}
