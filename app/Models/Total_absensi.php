<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Total_absensi extends Model
{
    protected $table = 'total_absensi';

    protected $fillable = [
        'user_id',
        'tanggal',
        'jmlh_hari',
        'jmlh_jam',
        'jmlh_sakit',
        'jmlh_cuti',
        'jmlh_izin',
        'jmlh_alpha'
    ];
}
