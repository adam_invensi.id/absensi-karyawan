<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMdepartemen extends Model
{
    protected $table = 'tm_departemen';

    protected $fillable = [
        'departemen'
    ];
}
