<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMjabatan extends Model
{
    protected $table = 'tm_jabatan';

    protected $fillable = [
        'departemen_id',
        'jabatan'
    ];
}
