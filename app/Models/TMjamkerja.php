<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TMjamkerja extends Model
{
    protected $table = 'tm_jam_kerja';

    protected $fillable = [
        'jam',
        'jam_mulai',
        'jam_selesai'
    ];
}
