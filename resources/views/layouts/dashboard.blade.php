@extends('layouts.main')
@section('content')
<div class="intro-y box p-5">
    <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
        <form class="form form-signup" method="GET" action="{{route('absen.absen-hari-ini')}}" autocomplete="off">
            @csrf
            <fieldset>
                <legend>Please, enter your email, password and password confirmation for sign up.</legend>
                <div class="input-block">
                    <label for="signup-email">E-mail</label>
                    <input id="signup-email" name="email" type="email" required>
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="input-block">
                    <label for="signup-password" class="required">Absen</label>
                    <select name="absen" class="form-select" id="signup-password">
                        @foreach($absen as $indeks => $ab)
                        <option value="{{ $ab->id }}">{{ $ab->absen }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('absen'))
                    <span class="text-danger">{{ $errors->first('absen') }}</span>
                    @endif
                </div>
                <div class="input-block">
                    <label for="tanggal" class="required">Tanggal</label>
                    <input id="tanggal" type="date" name="tanggal">
                    @if ($errors->has('tanggal'))
                    <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                    @endif
                </div>
                <div class="input-block">
                    <label for="jam" class="required">Jam</label>
                    <input type="time" name="jam" id="jam">
                    @if ($errors->has('jam'))
                    <span class="text-danger">{{ $errors->first('jam') }}</span>
                    @endif
                </div>
                <div class="input-block">
                    <label for="signup-password" class="required">Absen</label>
                    <select name="lokasi" id="lokasi" class="form-select">
                        @foreach($lokasi as $index => $lk)
                        <option value="{{ $lk->id }}">{{ $lk->lokasi }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('lokasi'))
                    <span class="text-danger">{{ $errors->first('lokasi') }}</span>
                    @endif
                    @if ($errors->has('lainnya'))
                    <span class="text-danger">{{ $errors->first('lainnya') }}</span>
                    @endif
                </div>
                <div class="input-block input-hidden">
                    <label for="lainnya" class="required">Lainnya</label>
                    <input type="text" name="lainnya" id="lainnya">
                </div>
                <div class="input-block">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" name="keterangan" id="keterangan">
                </div>
            </fieldset>
            <button type="submit" class="btn-login">Kirim</button>
        </form>
    </div>
</div>
<div class="col-span-12 lg:col-span-8 2xl:col-span-9">
    <div class="intro-y box ">
        <div class="flex items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">
            <h2 class="font-medium text-base mr-auto">
                Info Pengguna
            </h2>
        </div>
        <div class="p-5">
            <form method="post">
            <div class="flex flex-col-reverse xl:flex-row flex-col">
                    @csrf
                    
                    <div class="flex-1 mt-6 xl:mt-0">
                        <div class="grid grid-cols-12 gap-x-5">
                            <div class="col-span-12 2xl:col-span-6">
                                <div>
                                    <input id="update-profile-form-1" type="text" name="user_id" class="form-control" value="{{ $user->id }}" hidden>
                                    <input id="update-profile-form-1" type="text" name="biodata_id" class="form-control" value="{{ $user->biodata_id }}" hidden> 
                                    <label for="update-profile-form-1" class="form-label">Nama Lengkap</label>
                                    <input id="update-profile-form-1" type="text" name="nama" class="form-control" placeholder="Nama Lengkap..." value="{{ $user->nama }}">
                                    @if ($errors->has('nama'))
                                    <span class="text-danger">{{ $errors->first('nama') }}</span>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-1" class="form-label">Username</label>
                                    <input id="update-profile-form-1" type="text" name="username" class="form-control" placeholder="Username..." value="{{ $user->username }}">
                                    @if ($errors->has('username'))
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-2" class="form-label">Posisi</label>
                                    <select id="update-profile-form-2" name="posisi" data-search="true" class="tom-select w-full">
                                        @foreach($posisi as $indeks => $pss)
                                        @if ($user->posisi == $pss->posisi)
                                        <option value="{{ $pss->id }}" selected>{{ $pss->posisi }}</option>
                                        @endif
                                        <option value="{{ $pss->id }}">{{ $pss->posisi }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('posisi'))
                                    <span class="text-danger">{{ $errors->first('posisi') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-span-12 2xl:col-span-6">
                                <div class="mt-3 2xl:mt-0">
                                    <label for="update-profile-form-3" class="form-label">Jabatan</label>
                                    <select id="update-profile-form-3" name="departemen" data-search="true" class="tom-select w-full">
                                        @foreach($departemen as $index => $jabatan)
                                        @if ($user->jabatan == $jabatan->jabatan)
                                        <option value="{{ $jabatan->id }}" selected>{{ $jabatan->jabatan }}</option>
                                        @endif
                                        <option value="{{ $jabatan->id }}">{{ $jabatan->jabatan }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('departemen'))
                                    <span class="text-danger">{{ $errors->first('departemen') }}</span>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-4" class="form-label">Nomor Handphone</label>
                                    <input id="update-profile-form-4" type="text" name="no_telepon" class="form-control" placeholder="08*********" value="{{ $user->no_telepon }}">
                                    @if ($errors->has('no_telepon'))
                                    <span class="text-danger">{{ $errors->first('no_telepon') }}</span>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-6" class="form-label">Jenis Kelamin</label>
                                    @if ($user->jenis_kelamin == "Laki-laki")
                                        
                                    <div class="form-check mr-2"> <input id="radio-switch-4" class="form-check-input" type="radio" name="jenis_kelamin" value="Laki-laki" checked> <label class="form-check-label" for="radio-switch-4">Laki-laki</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="radio-switch-5" class="form-check-input" type="radio" name="jenis_kelamin" value="Perempuan"> <label class="form-check-label" for="radio-switch-5">Perempuan</label> </div>
                                    @elseif($user->jenis_kelamin == "Perempuan")
                                    <div class="form-check mr-2"> <input id="radio-switch-4" class="form-check-input" type="radio" name="jenis_kelamin" value="Laki-laki"> <label class="form-check-label" for="radio-switch-4">Laki-laki</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="radio-switch-5" class="form-check-input" type="radio" name="jenis_kelamin" value="Perempuan" checked> <label class="form-check-label" for="radio-switch-5">Perempuan</label> </div>
                                    @endif
                                </div>
                                @if ($errors->has('jenis_kelamin'))
                                <span class="text-danger">{{ $errors->first('jenis_kelamin') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12">
                                <div class="mt-3">
                                    <label for="update-profile-form-5" class="form-label">Alamat</label>
                                    <input type="text" name="alamat" id="update-profile-form-5" class="form-control" placeholder="Alamat" value="{{ $user->alamat }}">
                                    @if ($errors->has('alamat'))
                                    <span class="text-danger">{{ $errors->first('alamat') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <button type="submit" formaction="{{ route('MasterUser.show', auth()->user()->id) }}" class="btn btn-secondary w-20 mt-4">Back</button>
                        <button type="submit" formaction="{{ route('MasterUser.update-pengaturan-pengguna') }}" class="btn btn-primary w-20 mt-4 ml-1">Save</button>

                    </div>
                    <div class="w-52 mx-auto xl:mr-0 xl:ml-6">
                        <div class="border-2 border-dashed shadow-sm border-slate-200/60 dark:border-darkmode-400 rounded-md p-5">
                            <div class="h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                <img class="w-full rounded-md" data-action="zoom" alt="Midone - HTML Admin Template" src="{{ url('assets/dist/images/profile-11.jpg') }}">

                                <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-danger right-0 top-0 -mr-2 -mt-2"> <i data-lucide="x" class="w-4 h-4"></i> </div>
                            </div>
                            <div class="mx-auto cursor-pointer relative mt-5">
                                <button type="submit" formaction="{{ route('MasterUser.ganti-foto') }}" class="btn btn-primary w-full">Ganti Foto</button>
                                <input type="file" name="photo" class="w-full h-full top-0 left-0 absolute opacity-0">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END: Display Information -->
</div>
@endsection