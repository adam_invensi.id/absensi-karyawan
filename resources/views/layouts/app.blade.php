<!doctype html>
<html lang="en" class="dark">
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="{{ url('assets/dist/images/invensi.png') }}" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Enigma admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Enigma Admin Template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="LEFT4CODE">
    <title>Login - Absensi Karyawan - Invensi.id</title>
    <!-- BEGIN: CSS Assets-->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <!-- END: CSS Assets-->
    @stack('style')
</head>
<!-- END: Head -->
<body class="login">
    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
        @yield('content')
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/login.js') }}" defer></script>

    <!--   Core JS Files   -->
    <script src="{{ asset('material') }}/js/core/jquery.min.js"></script>
    <script src="{{ asset('material') }}/js/core/popper.min.js"></script>
    <script src="{{ asset('material') }}/js/core/bootstrap-material-design.min.js"></script>
    <script src="{{ asset('material') }}/js/plugins/bootstrap-selectpicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script src="{{ asset('material') }}/js/material-dashboard.js" type="text/javascript"></script>
    
    <script src="{{ asset('/') }}js/moment.js"></script>
    <script>
        moment.locale('ID'); 
    </script>
    @stack('js')
</body>
</html>
