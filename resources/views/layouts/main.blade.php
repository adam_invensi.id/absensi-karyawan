@php
use App\Models\User;
    $user = User::select('users.*', 'tm_biodata.user_id', 'tm_biodata.jabatan_id', 'tm_biodata.no_telepon', 'tm_biodata.jenis_kelamin', 'tm_biodata.tempat_lahir', 'tm_biodata.tanggal_lahir', 'tm_biodata.alamat', 'tm_biodata.agama', 'tm_jabatan.departemen_id', 'tm_jabatan.jabatan', 'tm_departemen.departemen')
    ->join('tm_biodata', 'users.id', '=', 'tm_biodata.user_id')
    ->join('tm_jabatan', 'tm_biodata.jabatan_id', '=', 'tm_jabatan.id')
    ->join('tm_departemen', 'tm_jabatan.departemen_id', '=', 'tm_departemen.id')
    ->where('users.id', auth()->user()->id)
    ->first();
    
@endphp
<!DOCTYPE html>
<html lang="en" class="dark">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="dist/images/logo.svg" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Enigma admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, Enigma Admin Template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="LEFT4CODE">
        <title>Dashboard - Absensi Karyawan</title>
        <!-- BEGIN: CSS Assets-->
        <link href="{{ url('assets/dist/css/app.css') }}" rel="stylesheet">
        <!-- END: CSS Assets-->
        @yield('css')

        <style type="text/css">
            /* Google font */
            @import url('https://fonts.googleapis.com/css?family=Orbitron');
                
            #digit_clock_time_mobile {
                font-family: 'Work Sans', sans-serif;
                color: #ffffff;
                font-size: 26px;
                text-align: center;
            }
            #digit_clock_time {
                font-family: 'Work Sans', sans-serif;
                color: #ffffff;
                font-size: 26px;
                text-align: center;
            }
        </style>
    </head>

    <body class="py-5 md:py-0">
        <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="" class="flex mr-auto">
                    <img alt="Invensi.id" class="logo__image w-20" src="{{ url('assets/dist/images/invensi.png') }}">
                </a>
                <a href="javascript:;" class="mobile-menu-toggler"> <i data-lucide="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <div class="scrollable">
                <a href="javascript:;" class="mobile-menu-toggler"> <i data-lucide="x-circle" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
                <ul class="scrollable__content py-2">
                    <li>
                        <div class="menu menu__title">
                            <div id="digit_clock_time_mobile"></div>
                        </div>
                    </li>
                    @if (auth()->user()->role_id == 1)
                        <li>
                            <div class="menu__title"> Administrator </div>
                        </li>
                        <li>
                            <a href="{{ route('home') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="home"></i> </div>
                                <div class="menu__title"> Dashboard </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('MasterUser.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="users"></i> </div>
                                <div class="menu__title"> Karyawan </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('JamKerja.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="alarm-clock"></i> </div>
                                <div class="menu__title"> Jam Kerja </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Departemen.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="briefcase"></i> </div>
                                <div class="menu__title"> Departemen </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Jabatan.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="briefcase"></i> </div>
                                <div class="menu__title"> Jabatan </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Liburnas.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="calendar"></i> </div>
                                <div class="menu__title"> Libur Nasional </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Absen.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                                <div class="menu__title"> Absensi </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Report.index') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                                <div class="menu__title"> Report </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="power"></i> </div>
                                <div class="menu__title"> Log Out </div>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->role_id == 2)
                        <li>
                            <div class="menu__title ml-2"> Users </div>
                        </li>
                        <li>
                            <a href="{{ route('home') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="home"></i> </div>
                                <div class="menu__title"> Dashboard </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('MasterUser.show', auth()->user()->id) }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="user"></i> </div>
                                <div class="menu__title"> Profil </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/absensi/absensi-hari-ini/') }}/{{ auth()->user()->id }}/show" class="menu">
                                <div class="menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                                <div class="menu__title"> Absensi </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}" class="menu">
                                <div class="menu__icon"> <i data-lucide="power"></i> </div>
                                <div class="menu__title"> Log Out </div>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- END: Mobile Menu -->
        <div class="top-bar-boxed h-[70px] md:h-[65px] z-[51] border-b border-white/[0.08] mt-12 md:mt-0 -mx-3 sm:-mx-8 md:-mx-0 px-3 md:border-b-0 relative md:fixed md:inset-x-0 md:top-0 sm:px-8 md:px-10 md:pt-10 md:bg-gradient-to-b md:from-slate-100 md:to-transparent dark:md:from-darkmode-700">
            <div class="h-full flex items-center">
                <!-- BEGIN: Logo -->
                <a href="" class="logo -intro-x hidden md:flex xl:w-[180px] block">
                    <img alt="Invensi.id" class="logo__image w-20" src="{{ url('assets/dist/images/invensi.png') }}">
                    <span class="logo__text text-white text-lg ml-3 mt-2"> invensi </span> 
                </a>
                <!-- END: Logo -->
                <!-- BEGIN: Breadcrumb -->
                <nav aria-label="breadcrumb" class="-intro-x h-[45px] mr-auto">
                    <ol class="breadcrumb breadcrumb-light">
                        <li class="breadcrumb-item"><a href="#">Application</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <div class="intro-x dropdown w-8 h-8">
                    <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in scale-110" role="button" aria-expanded="false" data-tw-toggle="dropdown">
                        <img alt="Midone - HTML Admin Template" src="{{ url('assets/dist/images/profile-3.jpg') }}">
                    </div>
                    <div class="dropdown-menu w-56">
                        <ul class="dropdown-content bg-primary/80 before:block before:absolute before:bg-black before:inset-0 before:rounded-md before:z-[-1] text-white">
                            <li class="p-2">
                                <div class="font-medium">{{ auth()->user()->nama }}</div>
                                <div class="text-xs text-white/60 mt-0.5 dark:text-slate-500">{{ ($user != null) ? $user->jabatan : '' }}</div>
                            </li>
                            <li>
                                <hr class="dropdown-divider border-white/[0.08]">
                            </li>
                            <li>
                                <a href="{{ route('MasterUser.show', auth()->user()->id) }}" class="dropdown-item hover:bg-white/5"> <i data-lucide="user" class="w-4 h-4 mr-2"></i> Profil </a>
                            </li>
                            <li>
                                <a href="{{ route('MasterUser.change-password', auth()->user()->id) }}" class="dropdown-item hover:bg-white/5"> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Ganti Password </a>
                            </li>
                            <li>
                                <hr class="dropdown-divider border-white/[0.08]">
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}" class="dropdown-item hover:bg-white/5"> <i data-lucide="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END: Account Menu -->
            </div>
        </div>

        <div class="flex overflow-hidden">
            <!-- BEGIN: Side Menu -->
            <nav class="side-nav">
                <ul>
                    <li>
                        <div class="side-menu side-menu__title">
                            <div id="digit_clock_time"></div>
                        </div>
                    </li>
                    @if (auth()->user()->role_id == 1)
                    <li>
                        <div class="side-menu__title"> Administrator </div>
                    </li>
                    <li>
                        <a href="{{ route('home') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('MasterUser.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="users"></i> </div>
                            <div class="side-menu__title"> Karyawan </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('JamKerja.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="alarm-clock"></i> </div>
                            <div class="side-menu__title"> Jam Kerja </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Departemen.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="briefcase"></i> </div>
                            <div class="side-menu__title"> Departemen </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Jabatan.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="briefcase"></i> </div>
                            <div class="side-menu__title"> Jabatan </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Liburnas.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="calendar"></i> </div>
                            <div class="side-menu__title"> Libur Nasional </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Absen.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                            <div class="side-menu__title"> Absensi </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Report.index') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                            <div class="side-menu__title"> Report </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="power"></i> </div>
                            <div class="side-menu__title"> Log Out </div>
                        </a>
                    </li>
                    @endif
                    @if (auth()->user()->role_id == 2)
                    <li>
                        <div class="side-menu__title"> Users </div>
                    </li>
                    <li>
                        <a href="{{ route('home') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('MasterUser.show', auth()->user()->id) }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="user"></i> </div>
                            <div class="side-menu__title"> Profil </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/absensi/absensi-hari-ini/') }}/{{ auth()->user()->id }}/show" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="clipboard-list"></i> </div>
                            <div class="side-menu__title"> Absensi </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="power"></i> </div>
                            <div class="side-menu__title"> Log Out </div>
                        </a>
                    </li>
                    @endif
                </ul>
            </nav>
            <!-- END: Side Menu -->
            <div class="content">
                @yield('content')
            </div>
        </div>

        <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        {{-- <script src="https://maps.googleapis.com/maps/api/js?key=["your-google-map-api"]&libraries=places"></script> --}}
        {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
        <script src="{{ url('assets/dist/js/app.js') }}"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <script src="{{ asset('/') }}js/moment.js"></script>
        <script>
            moment.locale('ID'); 
        </script>
        <!-- END: JS Assets-->
        <script type="text/javascript">
        const clock_mobile= document.getElementById("digit_clock_time_mobile");
        const clock= document.getElementById("digit_clock_time");
        function updateTime() {
            const now = moment();
            const humanReadable = now.format('hh:mm:ss A');

            clock.textContent = humanReadable;
            clock_mobile.textContent = humanReadable;

        }
        
        setInterval(updateTime, 1000);
        updateTime();
        </script> 
        @yield('js')
    </body>


</html>