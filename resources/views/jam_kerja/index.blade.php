@extends('layouts.main')

@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
</div>
<div class="p-10 w-100 box">
    <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
        Data Jam Kerja
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>Keterangan</th>
                        <th>Jam Mulai</th>
                        <th>Jam Selesai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($tmjamkerja as $indeks => $jamkerja)    
                    <tr class="intro-x">
                        <td>{{ $no }}</td>
                        <td>{{ $jamkerja->jam }}</td>
                        <td>{{ $jamkerja->jam_mulai }}</td>
                        <td>{{ $jamkerja->jam_selesai }}</td>
                        <td class="table-report__action w-56">
                            <div class="flex">
                                <a class="flex mr-3 edit-data" href="javascript:;" data-tw-toggle="modal" data-tw-target="#edit-modal-preview" data-id="{{ $jamkerja->id }}" data-jam="{{ $jamkerja->jam }}" data-mulai="{{ $jamkerja->jam_mulai }}" data-selesai="{{ $jamkerja->jam_selesai }}"> <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                            </div>
                        </td>
                    </tr>
                    @php
                        $no++
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
    </div>
</div>
{{-- Edit Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="edit-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form id="form-edit">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Edit Jam
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Keterangan</div>
                                </div>
                                <input type="hidden" name="jam_id" id="jam-id" class="form-control">
                                <input type="text" id="keterangan" name="keterangan" class="form-control" disabled>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Jam Mulai</div>
                                </div>
                                <input type="time" name="jam_mulai" id="mulai" class="form-control">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Jam Selesai</div>
                                </div>
                                <input type="time" name="jam_selesai" id="selesai" class="form-control">
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Edit Modal Content --}}

<!-- BEGIN: Modal Failed Form Content -->
<div id="failed-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Ada sesuatu yang salah, Mohon ulangi.</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Form Content -->

<!-- BEGIN: Modal Success Form Content -->
<div id="success-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Semoga harimu menyenangkan! :D</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Form Content -->
@endsection

@section('js')
<script>
    $(".edit-data").on('click', function(e){
        var id= $(this).data('id');
        var keterangan= $(this).data('jam');
        var jam_mulai= $(this).data('mulai');
        var jam_selesai= $(this).data('selesai');
        $('#jam-id').val(id);
        $('#keterangan').val(keterangan);
        $('#mulai').val(jam_mulai);
        $('#selesai').val(jam_selesai);
    });
    
    $(".button-success").on('click', function(e){
        e.preventDefault();
        location.reload(true);
    });

    $("#form-edit").on('submit', function(e){
        e.preventDefault();
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const editModal = tailwind.Modal.getInstance(document.querySelector("#edit-modal-preview"));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('Jam.updateJam') }}",
            data: {
                _method:"POST",
                id: $('#jam-id').val(),
                keterangan: $('#keterangan').val(),
                mulai: $('#mulai').val(),
                selesai: $('#selesai').val()
            },
            success: function(response) {
                if (response.errors) {
                    jQuery('.alert-danger').html('');

                    jQuery.each(response.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>'+value+'</li>');
                    });
                    return false;
                }
                if (response.status == 'false') {
                    // return false;
                    $('#registerForm errorList').html(JSON.stringify(response.errors));
                    editModal.hide();
                    failedModal.show();
                } else {
                    // Success notification 
                    editModal.hide();
                    successModal.show();
                }
            },
            error: function(error) {
                console.log(error)
                editModal.hide();
                failedModal.show();
            }
        });
    })
</script>
@endsection