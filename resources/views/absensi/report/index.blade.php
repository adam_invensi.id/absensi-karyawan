@extends('layouts.main')

@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
</div>
<div class="p-10 w-100 box">
    <ul class="nav nav-tabs" role="tablist">
        <li id="example-1-tab" class="nav-item flex-1" role="presentation"> 
            <button class="nav-link w-full py-2 active" data-tw-toggle="pill" data-tw-target="#example-tab-1" type="button" role="tab" aria-controls="example-tab-1" aria-selected="true">Export Report </button> 
        </li>
        <li id="example-2-tab" class="nav-item flex-1" role="presentation"> 
            <button class="nav-link w-full py-2" data-tw-toggle="pill" data-tw-target="#example-tab-2" type="button" role="tab" aria-controls="example-tab-2" aria-selected="false"> Import Report </button> 
        </li>
    </ul>
    <div class="tab-content border-l border-r border-b">
        <div id="example-tab-1" class="tab-pane leading-relaxed p-5 active" role="tabpanel" aria-labelledby="example-1-tab">
            <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
                Export Report
            </h2>
            <form action="{{ route('Report.export-report') }}" method="post">
                @csrf
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">
                    <div class="font-medium required text-center">Dari Tanggal : </div>
                    <input name="dari_tanggal" type="date" class="form-control w-40 text-center">
                    @if ($errors->has('dari_tanggal'))
                    <br>
                    <span class="text-danger">{{ $errors->first('dari_tanggal') }}</span>
                    @endif
                </h2>
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">Sampai Tanggal :</h2>
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">
                    <input name="sampai_tanggal" type="date" class="form-control w-40 text-center">
                    @if ($errors->has('sampai_tanggal'))
                    <br>
                    <span class="text-danger">{{ $errors->first('sampai_tanggal') }}</span>
                    @endif
                </h2>
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">Hari :</h2>
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">
                    <input name="hari" type="radio" id="sen-jum" value="1" class="form-control w-4 text-center">
                    <label for="sen-jum">Senin-Jumat</label><br>
                    <input name="hari" type="radio" id="sen-sab" value="2" class="form-control w-4 text-center">
                    <label for="sen-sab">Senin-Sabtu</label>
                    @if ($errors->has('hari'))
                    <br>
                    <span class="text-danger">{{ $errors->first('hari') }}</span>
                    @endif
                </h2>
                <h2 class="intro-x mt-2 text-center">
                    <button type="submit" class="btn btn-primary shadow-md mr-2"> <i data-lucide="share" class="w-4 h-4 mr-2"></i> Export </button>
                </h2>
            </form>
        </div>
        <div id="example-tab-2" class="tab-pane leading-relaxed p-5" role="tabpanel" aria-labelledby="example-2-tab"> 
            <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
                Import Report
            </h2>
            <form method="post" enctype="multipart/form-data" action="{{ route('Report.import-report') }}">
                @csrf
                <h2 class="intro-x font-bold text-xl xl:text-xl text-center">
                    <div class="font-medium required text-center"> </div>
                    <input name="file_import" type="file" class="form-control w-40 text-center">
                    <input type="hidden" name="idoldExcel" id="" value="{{ isset($ttexcel) ? $ttexcel->id : '' }}">
                    <input type="hidden" name="oldExcel" id="" value="{{ isset($ttexcel) ?$ttexcel->file_tmp : '' }}">
                    @if ($errors->has('tanggal'))
                    <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                    @endif
                </h2>
                <h2 class="intro-x mt-2 text-center">
                    <button type="submit" class="btn btn-primary shadow-md mr-2"> <i data-lucide="import" class="w-4 h-4 mr-2"></i> Import </button>
                    
                    <button type="submit" formaction="{{ route('Report.get-download') }}" class="btn btn-primary shadow-md mr-2"> <i data-lucide="import" class="w-4 h-4 mr-2"></i> Download Contoh Import </button>
                </h2>
            </form> 
        </div>
    </div>

</div>
@endsection