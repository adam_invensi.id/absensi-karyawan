@extends('layouts.main')

@section('content')
<style>
    .form-filter {
        display: none;
    }
    .form-export {
        display: none;
    }
</style>
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
</div>
<!-- BEGIN: HTML Table Data -->
<div class="intro-y box p-5">
    <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
        Detail Absen<br>
        <p class="text-lg font-medium mr-auto mb-2">
            Nama : {{" " . $users->nama }}<br>
            Divisi : {{" " . $users->jabatan }}
        </p>
    </h2>
    <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
        <div class="dropdown w-1/2 sm:w-auto mr-auto mb-2">
            <button class="dropdown-toggle btn btn-outline-secondary w-full sm:w-auto" aria-expanded="false" data-tw-toggle="dropdown"> <i data-lucide="more-vertical" class="w-4 h-4 mr-2"></i> Action <i data-lucide="chevron-down" class="w-4 h-4 ml-auto sm:ml-2"></i> </button>
            <div class="dropdown-menu w-40">
                <ul class="dropdown-content">
                    <li>
                        <a class="dropdown-item filter-button"> <i data-lucide="filter" class="w-4 h-4 mr-2"></i> Filter </a>
                    </li>
                    <li>
                        <a class="dropdown-item export-button"> <i data-lucide="file-text" class="w-4 h-4 mr-2"></i> Export </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="form-filter">
            <form action="{{ route('Absen.filter-by-month') }}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ $users->id }}">
                <div class="flex mt-5 sm:mt-0">
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                    <select name="bulan" id="bln" class="form-select">
                        @if (isset($bulan_selected))
                            @foreach ($bulan as $indeks => $value)
                            @if ($indeks == $bulan_selected)
                            <option value="{{ $indeks }}" selected>{{ $value }}</option>
                            @endif
                            <option value="{{ $indeks }}">{{ $value }}</option>
                            @endforeach
                        @else
                            @foreach ($bulan as $indeks => $value)
                            <option value="{{ $indeks }}">{{ $value }}</option>
                            @endforeach
                        @endif
                    </select>
                    </div>
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                        <input name="tahun" type="text" class="form-control w-20" placeholder="Tahun" value="{{ date('Y') }}">
                    </div>
                    <button type="submit" id="tabulator-print" class="btn btn-success w-1/2 sm:w-auto mr-2"> <i data-lucide="filter" class="w-4 h-4 mr-2"></i> Filter </button>
                    <a href="{{ url('/absensi/absensi-hari-ini/') }}/{{ auth()->user()->id }}/show" class="btn btn-secondary w-1/2 sm:w-auto mr-2"> Reset Filter </a>
                </div>
            </form>
        </div>
        <div class="form-export">
            <form id="form-excel" action="{{ route('Absen.export-excel-user') }}" method="get">
                @csrf
                <input type="hidden" name="id" value="{{ $users->id }}">
                <div class="flex mt-5 sm:mt-0">
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                    <select name="bulan" id="bln" class="form-select">
                        @foreach ($bulan as $indeks => $value)
                        <option value="{{ $indeks }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                    <input name="tahun" type="text" class="form-control w-20" placeholder="Tahun" value="{{ date('Y') }}">
                    </div>
                    <button type="submit" id="tabulator-print" class="btn btn-success w-1/2 sm:w-auto mr-2"> <i data-lucide="file-text" class="w-4 h-4 mr-2"></i> Export </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="overflow-x-auto scrollbar-hidden">
        <div class="overflow-x-auto">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="whitespace-nowrap">No.</th>
                        <th class="whitespace-nowrap">Keterangan</th>
                        <th class="whitespace-nowrap">Tanggal</th>
                        <th class="whitespace-nowrap">Jam Masuk</th>
                        <th class="whitespace-nowrap">Jam Pulang</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($karyawan as $indeks => $absen)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ ($absen->absen == "Pulang") ? "Hadir" : $absen->absen }}</td>
                        <td>{{ $absen->tanggal_absen }}</td>
                        <td>{{ ($absen->jam_masuk == null) ? "-" : $absen->jam_masuk}}</td>
                        <td>{{ ($absen->jam_pulang == null) ? "-" : $absen->jam_pulang }}</td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END: HTML Table Data -->
@endsection

@section('js')
    <script>
    $('#table-export-xlsx').on('click', function(e){
        e.preventDefault();
        if($('#thn').val()){
            window.location.href="{{ url('/absensi/absensi-hari-ini/exportExcelAll') }}"
        }
    })

    $('.filter-button').on('click', function(e){
        e.preventDefault();
        $(".form-filter").show();
        $(".form-export").hide();
    });
    
    $('.export-button').on('click', function(e){
        e.preventDefault();
        $(".form-export").show();
        $(".form-filter").hide();
    });
        
    $(".form-filter").hide();
    $(".form-export").hide();
    </script>
@endsection