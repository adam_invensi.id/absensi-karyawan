@extends('layouts.main')

@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    
</div>
<!-- BEGIN: HTML Table Data -->
<div class="intro-y box p-5">
    <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
        Data Karyawan
    </h2>  
    <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
        <h2 class="text-lg font-medium mr-auto"></h2>  
        <div class="form-export">
            <form id="form-excel" action="{{ route('Absen.export-excel-all') }}" method="get">
                @csrf
                <input type="hidden" name="id" value="{{ auth()->user()->id }}">
                <div class="flex mt-5 sm:mt-0">
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                    <select name="bulan" id="bln" class="form-select">
                        @foreach ($bulan as $indeks => $value)
                        <option value="{{ $indeks }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="dropdown input-block w-1/2 sm:w-auto mr-2">
                    <input name="tahun" type="text" class="form-control w-20" placeholder="Tahun" value="{{ date('Y') }}">
                    </div>
                    <button type="submit" id="tabulator-print" class="btn btn-success w-1/2 sm:w-auto mr-2"> <i data-lucide="file-text" class="w-4 h-4 mr-2"></i> Export </button>
                </div>
            </form>
        </div>
    </div>
    <div class="overflow-x-auto scrollbar-hidden">
        <div class="overflow-x-auto">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="whitespace-nowrap">No.</th>
                        <th class="whitespace-nowrap">Karyawan</th>
                        <th class="whitespace-nowrap">Kontak</th>
                        <th class="whitespace-nowrap">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($users as $indeks => $user)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $user->nama }}</td>
                        <td>
                            <a href="" class="font-medium whitespace-nowrap">Email : {{" " . $user->email }}</a> 
                            <div class="text-slate-500 text-xs whitespace-nowrap mt-0.5">Telepon : {{" " . $user->no_telepon }}</div>
                        </td>
                        <td>
                            <div class="flex mt-4 lg:mt-0">
                                <a href="{{ url('/absensi/absensi-hari-ini/') }}/{{ $user->id }}/show" class="btn btn-outline-success py-1 px-2">Detail</a>
                            </div>
                        </td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END: HTML Table Data -->
@endsection

@section('js')
    
<script>
    var date = new Date();
    var currentDate = date.toISOString().substring(0,10);
    var currentTime = date.toISOString().substring(11,16);

    document.getElementById('currentDate').value = currentDate;
    document.getElementById('currentTime').value = currentTime;
</script>
@endsection