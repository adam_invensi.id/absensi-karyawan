<form method="GET" action="{{route('absen.absen-hari-ini-ol')}}" autocomplete="off">
    @csrf
    <!-- BEGIN: Modal Header -->
        <div class="modal-header">
            <h2 class="font-medium text-base mr-auto">
                Absen hari ini
            </h2>
        </div>
        <!-- END: Modal Header -->
        <!-- BEGIN: Modal Body -->
        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
            <div class="col-span-12 sm:col-span-6">
                <div class="flex items-center mb-2">
                    <div class="font-medium required">E-mail</div>
                </div>
                <input id="update-profile-form-1" name="email" type="email" class="form-control" value="{{ auth()->user()->email }}">
                @if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="col-span-12 sm:col-span-6">
                <div class="flex items-center mb-2 showed-tanggal">
                    <div class="font-medium required">Tanggal</div>
                </div>
                <input id="currentDate" name="tanggal" type="date" class="form-control">
                @if ($errors->has('tanggal'))
                <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                @endif
            </div>
            <div class="col-span-12 sm:col-span-6">
                <div class="flex items-center mb-2">
                    <div class="font-medium required">Absen</div>
                </div>
                <select name="absen" data-search="true" class="tom-select w-full" id="absen-type">
                    @foreach($absen as $indeks => $ab)
                    <option value="{{ $ab->id }}">{{ $ab->absen }}</option>
                    @endforeach
                </select>
                @if ($errors->has('absen'))
                <span class="text-danger">{{ $errors->first('absen') }}</span>
                @endif
            </div>
            <div class="col-span-12 sm:col-span-6 jam-control">
                <div class="flex items-center mb-2">
                    <div class="font-medium required">Jam</div>
                </div>
                <input id="currentTime" name="jam" type="time" class="form-control">
                @if ($errors->has('jam'))
                <span class="text-danger">{{ $errors->first('jam') }}</span>
                @endif
            </div>
            <div class="col-span-12 sm:col-span-6">
                <div class="flex items-center mb-2">
                    <div class="font-medium required">Lokasi</div>
                </div>
                <select name="lokasi" id="lokasi" class="tom-select w-full">
                    @foreach($lokasi as $index => $lk)
                    <option value="{{ $lk->id }}">{{ $lk->lokasi }}</option>
                    @endforeach
                </select>
                @if ($errors->has('lokasi'))
                <span class="text-danger">{{ $errors->first('lokasi') }}</span>
                @endif
                @if ($errors->has('lainnya'))
                <span class="text-danger">{{ $errors->first('lainnya') }}</span>
                @endif
            </div>
            <div class="col-span-12 sm:col-span-6 input-hidden">
                <div class="flex items-center mb-2">
                    <div class="font-medium required">Lainnya</div>
                </div>
                <input type="text" name="lainnya" id="lainnya" class="form-control">
            </div>
            <div class="col-span-12 sm:col-span-6">
                <div class="flex items-center mb-2 hidden-keterangan">
                    <div class="font-medium required">Keterangan</div>
                </div>
                <input type="text" name="keterangan" id="keterangan" class="form-control">
                @if ($errors->has('keterangan'))
                <span class="text-danger">{{ $errors->first('keterangan') }}</span>
                @endif
            </div>
        </div>
        <!-- END: Modal Body -->
        <!-- BEGIN: Modal Footer -->
        <div class="modal-footer">
            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
            <button type="submit" class="btn btn-primary w-20">Send</button>
        </div>
    </form>