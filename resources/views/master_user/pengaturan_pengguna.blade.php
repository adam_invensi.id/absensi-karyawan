@extends('layouts.main')
@section('content')
<!-- BEGIN: Display Information -->
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
        <div class="intro-y box mt-5 lg:mt-0">
            <div class="relative flex items-center p-5">
                <div class="w-12 h-12 image-fit">
                    <img alt="Midone - HTML Admin Template" class="rounded-full" src="{{ url('assets/dist/images/profile-1.jpg') }}">
                </div>
                <div class="ml-4 mr-auto">
                    <div class="font-medium text-base">{{ $user->nama }}</div>
                    <div class="text-slate-500">{{ $user->jabatan }}</div>
                </div>
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center" href="{{ route('MasterUser.show', $user->id) }}"> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Info Pengguna </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Pengaturan Akun </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Ganti Password </a>
                <a class="flex items-center text-primary font-medium  mt-5" href="{{ route('MasterUser.pengaturan-akun', auth()->user()->id) }}"> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Pengaturan Pengguna </a>
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center" href=""> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Pengaturan Email </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Kartu Kredit Tersimpan </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Sosial Media </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Informasi Gaji </a>
            </div>
        </div>
    </div>

    <!-- Success Notification Content --> 
    
    <div id="success-notification-content" class="toastify-content hidden flex"> <i class="text-success" data-lucide="check-circle"></i> <div class="ml-4 mr-4"> <div class="font-medium">Perubahan Disimpan!</div> <div class="text-slate-500 mt-1">Silahkan dicek kembali untuk perubahan.</div> </div> </div> 
    <button id="success-notification-toggle" style="display: none" class="btn btn-primary">Show Notification</button>
    
    <!-- Failed Notification Content -->
    <div id="failed-notification-content" class="toastify-content hidden flex"> <i class="text-danger" data-lucide="x-octagon"></i> <div class="ml-4 mr-4"> <div class="font-medium">Perubahan Gagal Disimpan!</div> <div class="text-slate-500 mt-1">Silahkan lakukan cek kembali.</div> </div> </div> 
    <button id="failed-notification-toggle" style="display: none" class="btn btn-primary">Show Notification</button>
    
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div class="intro-y box ">
            <div class="flex items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">
                <h2 class="font-medium text-base mr-auto">
                    Info Pengguna
                </h2>
            </div>
            <div class="p-5">
                <form method="post" enctype="multipart/form-data" action="{{ route('MasterUser.update-pengaturan-pengguna') }}">
                    <div class="flex flex-col-reverse xl:flex-row flex-col">
                        @csrf
                        
                        <div class="flex-1 mt-6 xl:mt-0">
                            <div class="grid grid-cols-12 gap-x-5">
                                <div class="col-span-12 2xl:col-span-6">
                                    <div class="mt-3">
                                        
                                        <input id="update-profile-form-1" type="text" name="user_id" class="form-control" value="{{ $user->id }}" hidden>
                                        <input id="update-profile-form-1" type="text" name="biodata_id" class="form-control" value="{{ $user->biodata_id }}" hidden> 
                                        <label for="update-profile-form-1" class="form-label">Nama Lengkap</label>
                                        <input id="update-profile-form-1" type="text" name="nama" class="form-control" placeholder="Nama Lengkap..." value="{{ $user->nama }}">
                                        @if ($errors->has('nama'))
                                        <span class="text-danger">{{ $errors->first('nama') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">Tempat Lahir</label>
                                        <input id="update-profile-form-1" type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir..." value="{{ $user->tempat_lahir }}">
                                        @if ($errors->has('tempat_lahir'))
                                        <span class="text-danger">{{ $errors->first('tempat_lahir') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">Username</label>
                                        <input id="update-profile-form-1" type="text" name="username" class="form-control" placeholder="Username..." value="{{ $user->username }}">
                                        @if ($errors->has('username'))
                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">NPWP</label>
                                        <input id="update-profile-form-1" type="text" name="npwp" class="form-control" placeholder="NPWP..." value="{{ $user->npwp }}">
                                        @if ($errors->has('npwp'))
                                        <span class="text-danger">{{ $errors->first('npwp') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">NIK</label>
                                        <input id="update-profile-form-1" type="text" name="nik" class="form-control" placeholder="NIK..." value="{{ $user->nik }}">
                                        @if ($errors->has('nik'))
                                        <span class="text-danger">{{ $errors->first('nik') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-2" class="form-label">Agama</label>
                                        <select id="update-profile-form-2" name="agama" data-search="true" class="tom-select w-full">
                                            <option value="Islam" {{ ($user->agama == "Islam") ? 'selected' : ' ' }}>Islam</option>
                                            <option value="Kristen" {{ ($user->agama == "Kristen") ? 'selected' : ' ' }}>Kristen</option>
                                            <option value="Katholik" {{ ($user->agama == "Katholik") ? 'selected' : ' ' }}>Katholik</option>
                                            <option value="Hindu" {{ ($user->agama == "Hindu") ? 'selected' : ' ' }}>Hindu</option>
                                            <option value="Buddha" {{ ($user->agama == "Buddha") ? 'selected' : ' ' }}>Buddha</option>
                                            <option value="Khonghucu" {{ ($user->agama == "Khonghucu") ? 'selected' : ' ' }}>Khonghucu</option>
                                        </select>
                                        @if ($errors->has('agama'))
                                        <span class="text-danger">{{ $errors->first('agama') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-2" class="form-label">Upah</label>
                                        <select id="update-profile-form-2" name="upah" data-search="true" class="tom-select w-full">
                                            @foreach ($upah as $indx => $rupiah)
                                            @php
                                                $hasil_rupiah = "Rp " . number_format($rupiah->upah,2,',','.');
                                            @endphp
                                            <option value="{{ $rupiah->id }}" {{ ($rupiah->id == $user->upah_id) ? 'selected' : ' ' }}>{{ $hasil_rupiah }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('upah'))
                                        <span class="text-danger">{{ $errors->first('upah') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 2xl:col-span-6">
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">Tanggal Masuk Kerja </label>
                                        <div class="flex">
                                            <input id="update-profile-form-1" type="date" name="tmk" class="form-control w-30" placeholder="Tanggal Masuk Kerja..." value="{{ $user->tmk }}">
                                            <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Tanggal tersimpan    {{ $user->tmk }}</div>
                                        </div>
                                        @if ($errors->has('tmk'))
                                        <span class="text-danger">{{ $errors->first('tmk') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-1" class="form-label">Tanggal Lahir</label>
                                        <input id="update-profile-form-1" type="date" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir..." value="{{ $user->tanggal_lahir }}">
                                        @if ($errors->has('tanggal_lahir'))
                                        <span class="text-danger">{{ $errors->first('tanggal_lahir') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-2" class="form-label">Departemen</label>
                                        <select id="update-profile-form-2" name="jabatan" data-search="true" class="tom-select w-full">
                                            @foreach($departemen as $key => $value)
                                            <optgroup label="{{ $value->departemen }}">
                                                @foreach($jabatan as $key => $val)
                                                    @if($val->departemen_id == $value->id)
                                                    @if ($user->jabatan == $val->jabatan)
                                                    <option value="{{ $val->id }}" selected>{{ $val->jabatan }}</option>
                                                    @else
                                                    <option value="{{ $val->id }}">{{ $val->jabatan }}</option>
                                                    @endif
                                                    @endif
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('jabatan'))
                                        <span class="text-danger">{{ $errors->first('jabatan') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-4" class="form-label">Nomor Handphone</label>
                                        <input id="update-profile-form-4" type="text" name="no_telepon" class="form-control" placeholder="08*********" value="{{ $user->no_telepon }}">
                                        @if ($errors->has('no_telepon'))
                                        <span class="text-danger">{{ $errors->first('no_telepon') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-6" class="form-label">Jenis Kelamin</label>
                                        <div class="form-check mr-2"> 
                                            <input id="radio-switch-4" class="form-check-input" type="radio" name="jenis_kelamin" value="1" {{ ($user->jenis_kelamin == 1) ? 'checked' : ' ' }}> 
                                            <label class="form-check-label" for="radio-switch-4">Laki-laki</label> 
                                        </div>
                                        <div class="form-check mr-2 mt-2 sm:mt-0"> 
                                            <input id="radio-switch-5" class="form-check-input" type="radio" name="jenis_kelamin" value="2" {{ ($user->jenis_kelamin == 2) ? 'checked' : ' ' }}> 
                                            <label class="form-check-label" for="radio-switch-5">Perempuan</label> 
                                        </div>
                                        @if ($errors->has('jenis_kelamin'))
                                        <span class="text-danger">{{ $errors->first('jenis_kelamin') }}</span>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label for="update-profile-form-2" class="form-label">Status Perkawinan</label>
                                        <select id="update-profile-form-2" name="status_perkawinan" data-search="true" class="tom-select w-full">
                                            <option value="Belum Kawin" {{ ($user->status_perkawinan == "Belum Kawin") ? 'selected' : ' ' }}>Belum Kawin</option>
                                            <option value="Kawin" {{ ($user->status_perkawinan == "Kawin") ? 'selected' : ' ' }}>Kawin</option>
                                            <option value="Kawin 1" {{ ($user->status_perkawinan == "Kawin 1") ? 'selected' : ' ' }}>Kawin (Anak 1)</option>
                                            <option value="Kawin 2" {{ ($user->status_perkawinan == "Kawin 2") ? 'selected' : ' ' }}>Kawin (Anak 2)</option>
                                            <option value="Kawin 3" {{ ($user->status_perkawinan == "Kawin 3") ? 'selected' : ' ' }}>Kawin (Anak 3)</option>
                                            <option value="Cerai Hidup" {{ ($user->status_perkawinan == "Cerai Hidup") ? 'selected' : ' ' }}>Cerai Hidup</option>
                                            <option value="Cerai Mati" {{ ($user->status_perkawinan == "Cerai Mati") ? 'selected' : ' ' }}>Cerai Mati</option>
                                            <option value="Duda" {{ ($user->status_perkawinan == "Duda") ? 'selected' : ' ' }}>Duda</option>
                                            <option value="Janda" {{ ($user->status_perkawinan == "Janda") ? 'selected' : ' ' }}>Janda</option>
                                        </select>
                                        @if ($errors->has('status_perkawinan'))
                                        <span class="text-danger">{{ $errors->first('status_perkawinan') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12">
                                    <div class="mt-3">
                                        <label for="update-profile-form-5" class="form-label">Alamat</label>
                                        <input type="text" name="alamat" id="update-profile-form-5" class="form-control" placeholder="Alamat" value="{{ $user->alamat }}">
                                        @if ($errors->has('alamat'))
                                        <span class="text-danger">{{ $errors->first('alamat') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <a href="{{ route('MasterUser.show', auth()->user()->id) }}" class="btn btn-secondary w-20 mt-4">Back</a>
                            <button type="submit" class="btn btn-primary w-20 mt-4 ml-1">Save</button>

                        </div>
                        <div class="w-52 mx-auto xl:mr-0 xl:ml-6">
                            <div class="border-2 border-dashed shadow-sm border-slate-200/60 dark:border-darkmode-400 rounded-md p-5">
                                <div class="h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                    @if($user->photo)
                                        <img class="w-full rounded-md img-preview" data-action="zoom" src="{{ asset('storage/' . $user->photo) }}">
                                    @else 
                                        <img class="w-full rounded-md img-preview" data-action="zoom" alt="Midone - HTML Admin Template" src="{{ url('assets/dist/images/profile-11.jpg') }}">
                                    @endif
                                </div>
                                <div class="mx-auto cursor-pointer relative mt-5">
                                    <button type="button" class="btn btn-primary w-full">Ganti Foto</button>
                                    <input type="file" name="photo" id="photo-pre" class="w-full h-full top-0 left-0 absolute opacity-0" onchange="previewImage()">
                                    <input type="hidden" name="oldPhoto" id="" value="{{ $user->photo }}">

                                    @if ($errors->has('photo'))
                                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END: Display Information -->
    </div>
</div>
@endsection


@section('js')
<script>
    @if (session()->has('success'))
    $(document).ready(function(){
        $('#success-notification-toggle').trigger('click');
         // Success non sticky notification 
        Toastify({ node: $("#success-notification-content") .clone() .removeClass("hidden")[0], duration: 3000, newWindow: true, close: true, gravity: "top", position: "right", backgroundColor: "white", stopOnFocus: true, }).showToast();  
    })
    @endif
    @if (session()->has('failed'))
    $(document).ready(function(){
        $('#failed-notification-toggle').trigger('click');
        $("#failed-notification-toggle").on("click", function () { 
            // Failed non sticky notification 
            Toastify({ node: $("#failed-notification-content") .clone() .removeClass("hidden")[0], duration: 3000, newWindow: true, close: true, gravity: "top", position: "right", backgroundColor: "white", stopOnFocus: true, }).showToast();  
        }); 
    })
    @endif

    function previewImage() {
        const image = document.querySelector('#photo-pre');
        const imgPreview= document.querySelector('.img-preview');
    
        imgPreview.style.display = 'block';
        const oFReader= new FileReader();
        oFReader.readAsDataURL(image.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection