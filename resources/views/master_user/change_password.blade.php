@extends('layouts.main')

@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
        <div class="intro-y box mt-5 lg:mt-0">
            <div class="relative flex items-center p-5">
                <div class="w-12 h-12 image-fit">
                    <img alt="Midone - HTML Admin Template" class="rounded-full" src="{{ url('assets/dist/images/profile-1.jpg') }}">
                </div>
                <div class="ml-4 mr-auto">
                    <div class="font-medium text-base">{{ $user->nama }}</div>
                    <div class="text-slate-500">{{ ($user->jabatan) ? $user->jabatan : "-"  }}</div>
                </div>
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center" href="{{ route('MasterUser.show', $user->id) }}"> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Info Pengguna </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Pengaturan Akun </a>
                <a class="flex items-center mt-5 text-primary font-medium" href="{{ route('MasterUser.change-password', $user->id) }}"> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Ganti Password </a> 
                @if (auth()->user()->role_id == 1)
                <a class="flex items-center mt-5" href="{{ route('MasterUser.pengaturan-pengguna', $user->id) }}"> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Pengaturan Pengguna </a>
                @endif
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center" href=""> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Pengaturan Email </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Kartu Kredit Tersimpan </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Sosial Media </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Informasi Gaji </a>
            </div>
        </div>
    </div>
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Seller Details -->
            <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="box p-5 rounded-md">
                    <form id="form-data-user">
                        @csrf
                            <input type="hidden" id="user-id" value="{{ $user->id }}">
                            <label class="form-label required" for="password-existing">Kata sandi lama</label>
                            <input class="form-control" type="password" name="password_existing" id="password-existing" required>
                            @if ($errors->has('old_password'))
                            <span class="text-danger">{{ $errors->first('old_password') }}</span>
                            @endif
                            <label class="form-label required mt-5" for="new-password">Kata sandi baru</label>
                            <input class="form-control" type="password" name="new_password" id="new-password" required>

                            <label class="form-label required mt-5" for="confirmation-password">Konfirmasi kata sandi anda</label>
                            <input class="form-control" type="password" name="confirmation_password" id="confirmation-password" required>

                            <input type="checkbox" class="form-check-input" name="show_password" id="show-password">
                            <label class="form-label mt-5" for="show-password">Lihat kata sandi</label>
                        <div align="right">
                            <button type="button" class="btn btn-success btn-md" id="save-button"><i data-lucide="save" class="w-4 h-4 mr-1"></i>Simpan perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: Modal Failed Form Content -->
<div id="failed-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Tersimpan!</div>
                    <div class="text-slate-500 mt-2">
                        <span id="spanFailed"></span>
                    </div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 ok-failed">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Form Content -->

<!-- BEGIN: Modal Success Form Content -->
<div id="success-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Tersimpan!</div>
                    <div class="text-slate-500 mt-2">
                        @if (session()->has('success'))
                        {{ session('success') }}
                        @endif
                    </div>
                </div>
                <div class="px-5 pb-8 text-center"> <a href="{{ route('home') }}"><button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button></a> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Form Content -->
@endsection

@section('js')
<script>
    $('#show-password').click(function() {
        if ($('#show-password').is(':checked')) {
            $('#password-existing').attr('type', 'text');
            $('#new-password').attr('type', 'text');
            $('#confirmation-password').attr('type', 'text');
        } else {
            $('#password-existing').attr('type', 'password');
            $('#new-password').attr('type', 'password');
            $('#confirmation-password').attr('type', 'password');
        }
    })
</script>
<script>
    $('#button-success').click(function(e) {
        e.preventDefault();
        location.reload();
    });
    $('#save-button').click(function() {
        var data_user = {
            user_id: $('#user-id').val(),
            old_password: $('#password-existing').val(),
            new_password: $('#new-password').val(),
            confirmation_password: $('#confirmation-password').val(),
        };
        let token = $('input[name="_token"]').val();
        console.log(data_user, token);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            url: "{{ route('MasterUser.update-password') }}",
            data: data_user,
            success: function(response) {
                console.log(response.status)
                console.log(response.message)
                var message = response.message;
                if (response.status == 'errors'){
                    $("#spanFailed").html(message);
                    const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
                    failedModal.show();
                }else{
                    const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
                    successModal.show().then(function() {
                            window.location.href = "{{ route('MasterUser.show', $user->id) }}";
                        });
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 422) {
                    var message = ''
                    for (var i in jqXHR.responseJSON.errors) {
                        var d = jqXHR.responseJSON.errors[i]

                        for (var f in d) {
                            message += d[f] + '<br>'
                        }
                    }
                    console.log(message)
                    $("#spanFailed").html(message);
                    const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
                    failedModal.show().then(function() {
                        location.reload();
                    });
                }
            }
        });
    })
</script>
@endsection