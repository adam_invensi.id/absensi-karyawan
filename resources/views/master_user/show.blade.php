@extends('layouts.main')

@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <!-- BEGIN: Profile Menu -->
    <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
        <div class="intro-y box mt-5 lg:mt-0">
            <div class="relative flex items-center p-5">
                <div class="w-12 h-12 image-fit">
                    <img alt="Midone - HTML Admin Template" class="rounded-full" src="{{ url('assets/dist/images/profile-1.jpg') }}">
                </div>
                <div class="ml-4 mr-auto">
                    <div class="font-medium text-base">{{ $user->nama }}</div>
                    <div class="text-slate-500">{{ ($user->jabatan) ? $user->jabatan : "-"  }}</div>
                </div>
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center text-primary font-medium" href="{{ route('MasterUser.show', auth()->user()->id) }}"> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Info Pengguna </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Pengaturan Akun </a>
                <a class="flex items-center mt-5" href="{{ route('MasterUser.change-password', $user->id) }}"> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Ganti Password </a>
                @if (auth()->user()->role_id == 1)
                <a class="flex items-center mt-5" href="{{ route('MasterUser.pengaturan-pengguna', $user->id) }}"> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Pengaturan Pengguna </a>
                @endif
            </div>
            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                <a class="flex items-center" href=""> <i data-lucide="activity" class="w-4 h-4 mr-2"></i> Pengaturan Email </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="box" class="w-4 h-4 mr-2"></i> Kartu Kredit Tersimpan </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="lock" class="w-4 h-4 mr-2"></i> Sosial Media </a>
                <a class="flex items-center mt-5" href=""> <i data-lucide="settings" class="w-4 h-4 mr-2"></i> Informasi Gaji </a>
            </div>
        </div>
    </div>
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Seller Details -->
            <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="box p-5 rounded-md">
                    <div class="flex items-center border-b border-slate-200/60 dark:border-darkmode-400 pb-5 mb-5">
                        <div class="font-medium text-base truncate">Info Pengguna</div>
                    </div>
                    @php
                        $hasil_rupiah = "Rp " . number_format($user->upah,2,',','.');
                    @endphp
                    <div class="flex items-center"> <i data-lucide="clipboard" class="w-4 h-4 text-slate-500 mr-2"></i> Nama Lengkap: {{ " " .($user->nama) ? $user->nama : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="paperclip" class="w-4 h-4 text-slate-500 mr-2"></i> Departemen: {{ " " .($user->departemen) ? $user->departemen : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="paperclip" class="w-4 h-4 text-slate-500 mr-2"></i> Posisi: {{ " " .($user->jabatan) ? $user->jabatan : "-"  }} </div>
                    @if (auth()->user()->role_id == 1)
                    <div class="flex items-center mt-3"> <i data-lucide="banknote" class="w-4 h-4 text-slate-500 mr-2"></i> Upah: {{ " " .($hasil_rupiah) ? $hasil_rupiah : "-"  }} </div>
                    @endif
                    <div class="flex items-center mt-3"> <i data-lucide="user" class="w-4 h-4 text-slate-500 mr-2"></i> Username: {{ " " .($user->username) ? $user->username : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="credit-card" class="w-4 h-4 text-slate-500 mr-2"></i> NIK: {{ " " .($user->nik) ? $user->nik : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="credit-card" class="w-4 h-4 text-slate-500 mr-2"></i> NPWP: {{ " " .($user->npwp) ? $user->npwp : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="phone" class="w-4 h-4 text-slate-500 mr-2"></i> Nomor HP: {{ " " .($user->no_telepon) ? $user->no_telepon : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="map-pin" class="w-4 h-4 text-slate-500 mr-2"></i> Alamat: {{ " " .($user->alamat) ? $user->alamat : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="smile" class="w-4 h-4 text-slate-500 mr-2"></i> Jenis Kelamin: {{ " " .($user->jenis_kelamin) ? $user->jenis_kelamin : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="calendar" class="w-4 h-4 text-slate-500 mr-2"></i> TTL: {{ " " .($user->tempat_lahir) ? $user->tempat_lahir : "-"  }}/{{ " " .($user->tanggal_lahir) ? $user->tanggal_lahir : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="person-standing" class="w-4 h-4 text-slate-500 mr-2"></i> Agama: {{ " " .($user->agama) ? $user->agama : "-"  }} </div>
                    <div class="flex items-center mt-3"> <i data-lucide="person-standing" class="w-4 h-4 text-slate-500 mr-2"></i> Status Perkawinan: {{ " " .($user->status_perkawinan) ? $user->status_perkawinan : "-"  }} </div>
                </div>
            </div>
            <!-- END: Seller Details -->
            <!-- BEGIN: Daily Sales -->
            {{-- <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="flex items-center px-5 py-5 sm:py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Daily Sales
                    </h2>
                    <div class="dropdown ml-auto sm:hidden">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-lucide="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                        <div class="dropdown-menu w-40">
                            <ul class="dropdown-content">
                                <li>
                                    <a href="javascript:;" class="dropdown-item"> <i data-lucide="file" class="w-4 h-4 mr-2"></i> Download Excel </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <button class="btn btn-outline-secondary hidden sm:flex"> <i data-lucide="file" class="w-4 h-4 mr-2"></i> Download Excel </button>
                </div>
                <div class="p-5">
                    <div class="relative flex items-center">
                        <div class="w-12 h-12 flex-none image-fit">
                            <img alt="Midone - HTML Admin Template" class="rounded-full" src="dist/images/profile-1.jpg">
                        </div>
                        <div class="ml-4 mr-auto">
                            <a href="" class="font-medium">Kevin Spacey</a> 
                            <div class="text-slate-500 mr-5 sm:mr-5">Bootstrap 4 HTML Admin Template</div>
                        </div>
                        <div class="font-medium text-slate-600 dark:text-slate-500">+$19</div>
                    </div>
                    <div class="relative flex items-center mt-5">
                        <div class="w-12 h-12 flex-none image-fit">
                            <img alt="Midone - HTML Admin Template" class="rounded-full" src="dist/images/profile-5.jpg">
                        </div>
                        <div class="ml-4 mr-auto">
                            <a href="" class="font-medium">Tom Hanks</a> 
                            <div class="text-slate-500 mr-5 sm:mr-5">Tailwind HTML Admin Template</div>
                        </div>
                        <div class="font-medium text-slate-600 dark:text-slate-500">+$25</div>
                    </div>
                    <div class="relative flex items-center mt-5">
                        <div class="w-12 h-12 flex-none image-fit">
                            <img alt="Midone - HTML Admin Template" class="rounded-full" src="dist/images/profile-4.jpg">
                        </div>
                        <div class="ml-4 mr-auto">
                            <a href="" class="font-medium">Brad Pitt</a> 
                            <div class="text-slate-500 mr-5 sm:mr-5">Vuejs HTML Admin Template</div>
                        </div>
                        <div class="font-medium text-slate-600 dark:text-slate-500">+$21</div>
                    </div>
                </div>
            </div> --}}
            <!-- END: Daily Sales -->
            <!-- BEGIN: Announcement -->
            {{-- <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="flex items-center px-5 py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Pengumuman
                    </h2>
                    <button data-carousel="announcement" data-target="prev" class="tiny-slider-navigator btn btn-outline-secondary px-2 mr-2"> <i data-lucide="chevron-left" class="w-4 h-4"></i> </button>
                    <button data-carousel="announcement" data-target="next" class="tiny-slider-navigator btn btn-outline-secondary px-2"> <i data-lucide="chevron-right" class="w-4 h-4"></i> </button>
                </div>
                <div class="tiny-slider py-5" id="announcement">
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever. 
                            <br>
                            <br>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry since the 1500s.
                        </div>
                        <div class="flex items-center mt-5">
                            <div class="px-3 py-2 text-primary bg-primary/10 dark:bg-darkmode-400 dark:text-slate-300 rounded font-medium">02 June 2021</div>
                            <button class="btn btn-secondary ml-auto">View Details</button>
                        </div>
                    </div>
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever. 
                            <br>
                            <br>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry since the 1500s.
                        </div>
                        <div class="flex items-center mt-5">
                            <div class="px-3 py-2 text-primary bg-primary/10 dark:bg-darkmode-400 dark:text-slate-300 rounded font-medium">02 June 2021</div>
                            <button class="btn btn-secondary ml-auto">View Details</button>
                        </div>
                    </div>
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever. 
                            <br>
                            <br>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry since the 1500s.
                        </div>
                        <div class="flex items-center mt-5">
                            <div class="px-3 py-2 text-primary bg-primary/10 dark:bg-darkmode-400 dark:text-slate-300 rounded font-medium">02 June 2021</div>
                            <button class="btn btn-secondary ml-auto">View Details</button>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- END: Announcement -->
            <!-- BEGIN: Projects -->
            {{-- <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="flex items-center px-5 py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Projects
                    </h2>
                    <button data-carousel="projects" data-target="prev" class="tiny-slider-navigator btn btn-outline-secondary px-2 mr-2"> <i data-lucide="chevron-left" class="w-4 h-4"></i> </button>
                    <button data-carousel="projects" data-target="next" class="tiny-slider-navigator btn btn-outline-secondary px-2"> <i data-lucide="chevron-right" class="w-4 h-4"></i> </button>
                </div>
                <div class="tiny-slider py-5" id="projects">
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                        <div class="mt-5">
                            <div class="flex text-slate-500">
                                <div class="mr-auto">Pending Tasks</div>
                                <div>20%</div>
                            </div>
                            <div class="progress h-1 mt-2">
                                <div class="progress-bar w-1/2 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                        <div class="mt-5">
                            <div class="flex text-slate-500">
                                <div class="mr-auto">Pending Tasks</div>
                                <div>20%</div>
                            </div>
                            <div class="progress h-1 mt-2">
                                <div class="progress-bar w-1/2 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="px-5">
                        <div class="font-medium text-lg">Midone Admin Template</div>
                        <div class="text-slate-600 dark:text-slate-500 mt-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                        <div class="mt-5">
                            <div class="flex text-slate-500">
                                <div class="mr-auto">Pending Tasks</div>
                                <div>20%</div>
                            </div>
                            <div class="progress h-1 mt-2">
                                <div class="progress-bar w-1/2 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- END: Projects -->
            <!-- BEGIN: Work In Progress -->
            {{-- <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="flex items-center px-5 py-5 sm:py-0 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Work In Progress
                    </h2>
                    <div class="dropdown ml-auto sm:hidden">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-lucide="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                        <div class="nav nav-tabs dropdown-menu w-40" role="tablist">
                            <ul class="dropdown-content">
                                <li> <a id="work-in-progress-mobile-new-tab" href="javascript:;" data-tw-toggle="tab" data-tw-target="#work-in-progress-new" class="dropdown-item" role="tab" aria-controls="work-in-progress-new" aria-selected="true">New</a> </li>
                                <li> <a id="work-in-progress-mobile-last-week-tab" href="javascript:;" data-tw-toggle="tab" data-tw-target="#work-in-progress-last-week" class="dropdown-item" role="tab" aria-selected="false">Last Week</a> </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-link-tabs w-auto ml-auto hidden sm:flex" role="tablist" >
                        <li id="work-in-progress-new-tab" class="nav-item" role="presentation"> <a href="javascript:;" class="nav-link py-5 active" data-tw-target="#work-in-progress-new" aria-controls="work-in-progress-new" aria-selected="true" role="tab" > New </a> </li>
                        <li id="work-in-progress-last-week-tab" class="nav-item" role="presentation"> <a href="javascript:;" class="nav-link py-5" data-tw-target="#work-in-progress-last-week" aria-selected="false" role="tab" > Last Week </a> </li>
                    </ul>
                </div>
                <div class="p-5">
                    <div class="tab-content">
                        <div id="work-in-progress-new" class="tab-pane active" role="tabpanel" aria-labelledby="work-in-progress-new-tab">
                            <div>
                                <div class="flex">
                                    <div class="mr-auto">Pending Tasks</div>
                                    <div>20%</div>
                                </div>
                                <div class="progress h-1 mt-2">
                                    <div class="progress-bar w-1/2 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mt-5">
                                <div class="flex">
                                    <div class="mr-auto">Completed Tasks</div>
                                    <div>2 / 20</div>
                                </div>
                                <div class="progress h-1 mt-2">
                                    <div class="progress-bar w-1/4 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mt-5">
                                <div class="flex">
                                    <div class="mr-auto">Tasks In Progress</div>
                                    <div>42</div>
                                </div>
                                <div class="progress h-1 mt-2">
                                    <div class="progress-bar w-3/4 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mt-5">
                                <div class="flex">
                                    <div class="mr-auto">Tasks In Review</div>
                                    <div>70%</div>
                                </div>
                                <div class="progress h-1 mt-2">
                                    <div class="progress-bar w-4/5 bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <a href="" class="btn btn-secondary block w-40 mx-auto mt-5">View More Details</a> 
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- END: Work In Progress -->
            <!-- BEGIN: Latest Tasks -->
            {{-- <div class="intro-y box col-span-12 2xl:col-span-6">
                <div class="flex items-center px-5 py-5 sm:py-0 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Latest Tasks
                    </h2>
                    <div class="dropdown ml-auto sm:hidden">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-lucide="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                        <div class="nav nav-tabs dropdown-menu w-40" role="tablist">
                            <ul class="dropdown-content">
                                <li> <a id="latest-tasks-mobile-new-tab" href="javascript:;" data-tw-toggle="tab" data-tw-target="#latest-tasks-new" class="dropdown-item" role="tab" aria-controls="latest-tasks-new" aria-selected="true">New</a> </li>
                                <li> <a id="latest-tasks-mobile-last-week-tab" href="javascript:;" data-tw-toggle="tab" data-tw-target="#latest-tasks-last-week" class="dropdown-item" role="tab" aria-selected="false">Last Week</a> </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-link-tabs w-auto ml-auto hidden sm:flex" role="tablist" >
                        <li id="latest-tasks-new-tab" class="nav-item" role="presentation"> <a href="javascript:;" class="nav-link py-5 active" data-tw-target="#latest-tasks-new" aria-controls="latest-tasks-new" aria-selected="true" role="tab" > New </a> </li>
                        <li id="latest-tasks-last-week-tab" class="nav-item" role="presentation"> <a href="javascript:;" class="nav-link py-5" data-tw-target="#latest-tasks-last-week" aria-selected="false" role="tab" > Last Week </a> </li>
                    </ul>
                </div>
                <div class="p-5">
                    <div class="tab-content">
                        <div id="latest-tasks-new" class="tab-pane active" role="tabpanel" aria-labelledby="latest-tasks-new-tab">
                            <div class="flex items-center">
                                <div class="border-l-2 border-primary dark:border-primary pl-4">
                                    <a href="" class="font-medium">Create New Campaign</a> 
                                    <div class="text-slate-500">10:00 AM</div>
                                </div>
                                <div class="form-check form-switch ml-auto">
                                    <input class="form-check-input" type="checkbox">
                                </div>
                            </div>
                            <div class="flex items-center mt-5">
                                <div class="border-l-2 border-primary dark:border-primary pl-4">
                                    <a href="" class="font-medium">Meeting With Client</a> 
                                    <div class="text-slate-500">02:00 PM</div>
                                </div>
                                <div class="form-check form-switch ml-auto">
                                    <input class="form-check-input" type="checkbox">
                                </div>
                            </div>
                            <div class="flex items-center mt-5">
                                <div class="border-l-2 border-primary dark:border-primary pl-4">
                                    <a href="" class="font-medium">Create New Repository</a> 
                                    <div class="text-slate-500">04:00 PM</div>
                                </div>
                                <div class="form-check form-switch ml-auto">
                                    <input class="form-check-input" type="checkbox">
                                </div>
                            </div>
                            <div class="flex items-center mt-5">
                                <div class="border-l-2 border-primary dark:border-primary pl-4">
                                    <a href="" class="font-medium">Meeting With Client</a> 
                                    <div class="text-slate-500">10:00 AM</div>
                                </div>
                                <div class="form-check form-switch ml-auto">
                                    <input class="form-check-input" type="checkbox">
                                </div>
                            </div>
                            <div class="flex items-center mt-5">
                                <div class="border-l-2 border-primary dark:border-primary pl-4">
                                    <a href="" class="font-medium">Create New Repository</a> 
                                    <div class="text-slate-500">11:00 PM</div>
                                </div>
                                <div class="form-check form-switch ml-auto">
                                    <input class="form-check-input" type="checkbox">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- END: Latest Tasks -->
        </div>
    </div>
</div>
@endsection