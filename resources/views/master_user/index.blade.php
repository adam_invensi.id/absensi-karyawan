@extends('layouts.main')

@section('content')
<h2 class="mt-10 mb-5 font-bold text-2xl xl:text-3xl text-center">
    Users Layout
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <div class="text-center"> <a href="javascript:;" data-tw-toggle="modal" data-tw-target="#header-footer-modal-preview" class="btn btn-primary shadow-md mr-2">Tambah Karyawan</a></div>
        {{-- <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box" aria-expanded="false" data-tw-toggle="dropdown">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-lucide="plus"></i> </span>
            </button>
            <div class="dropdown-menu w-40">
                <ul class="dropdown-content">
                    <li>
                        <a href="" class="dropdown-item"> <i data-lucide="users" class="w-4 h-4 mr-2"></i> Add Group </a>
                    </li>
                    <li>
                        <a href="" class="dropdown-item"> <i data-lucide="message-circle" class="w-4 h-4 mr-2"></i> Send Message </a>
                    </li>
                </ul>
            </div>
        </div> --}}
        <div class="hidden md:block mx-auto text-slate-500"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-slate-500">
                <input type="text" class="form-control w-56 box pr-10" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i> 
            </div>
        </div>
    </div>
    <!-- BEGIN: Users Layout -->
    @foreach($users as $indeks => $dataUser)
    <div class="intro-y col-span-12 md:col-span-6">
        <div class="box">
            <div class="flex flex-col lg:flex-row items-center p-5">
                <div class="w-24 h-24 lg:w-12 lg:h-12 image-fit lg:mr-1">
                    <img alt="Midone - HTML Admin Template" class="rounded-full" src="{{ url('assets/dist/images/profile-2.jpg') }}">
                </div>
                <div class="lg:ml-2 lg:mr-auto text-center lg:text-left mt-3 lg:mt-0">
                    <a href="" class="font-medium">{{ $dataUser->nama }}</a> 
                    <div class="text-slate-500 text-xs mt-0.5">{{ $dataUser->role }}</div>
                </div>
                <div class="flex mt-4 lg:mt-0">
                    <a href="{{ route('MasterUser.show', $dataUser->id) }}" class="btn btn-outline-success py-1 px-2">Profile</a>
                    <a class="flex items-center ml-2 btn btn-outline-danger py-1 px-2 delete-data" href="javascript:;" data-id="{{ $dataUser->id }}" data-tw-toggle="modal" data-tw-target="#delete-confirmation-modal">Delete </a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- BEGIN: Users Layout -->
    <!-- END: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <nav class="w-full sm:w-auto sm:mr-auto">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevrons-left"></i> </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevron-left"></i> </a>
                </li>
                <li class="page-item"> <a class="page-link" href="#">...</a> </li>
                <li class="page-item"> <a class="page-link" href="#">1</a> </li>
                <li class="page-item active"> <a class="page-link" href="#">2</a> </li>
                <li class="page-item"> <a class="page-link" href="#">3</a> </li>
                <li class="page-item"> <a class="page-link" href="#">...</a> </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevron-right"></i> </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevrons-right"></i> </a>
                </li>
            </ul>
        </nav>
        <select class="w-20 form-select box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>

<!-- BEGIN: Modal Content -->
<div id="failed-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Ada sesuatu yang salah, Mohon ulangi.</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Content -->

<!-- BEGIN: Modal Content -->
<div id="success-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Semoga harimu menyenangkan! :D</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Content -->

<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="header-footer-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <form id="form-data" autocomplete="off">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Tambah Karyawan
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Alamat Email</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="modal-form-3" type="email" name="email" class="form-control" placeholder="contoh@invensi.id">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Password</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="password" name="password" class="form-control" placeholder="secret" autocomplete="off">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Nama Lengkap</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="modal-form-2" type="text" name="nama" class="form-control" placeholder="Nama Lengkap...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Username</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="modal-form-1" type="text" name="name" class="form-control" placeholder="Nama Panggilan..." autocomplete="off">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Peran/Role</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <select id="modal-form-6" class="form-select" name="role">
                                    @foreach($role as $index => $peran)
                                    <option value="{{ $peran->id }}">{{ $peran->role }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-form-5" class="form-label">Status</label>
                                <div class="mt-2">
                                    <div class="form-check form-switch"> <input id="checkbox-switch-7" class="form-check-input" type="checkbox" name="status"> <label class="form-check-label" for="checkbox-switch-7">Tidak Aktif/Aktif</label> </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-header">
                            <div class="row">
                                <h2 class="font-medium text-base mr-auto">
                                    Biodata
                                </h2>
                                <div class="alert alert-danger" style="display:none"></div>
                            </div>
                        </div>
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Departemen</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <select id="modal-form-6" class="form-select" name="departemen">
                                    @foreach($departemen as $key => $value)
                                    <optgroup label="{{ $value->departemen }}">
                                        @foreach($jabatan as $key => $val)
                                            @if($val->departemen_id == $value->id)
                                            <option value="{{ $val->id }}">{{ $val->jabatan }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Upah</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <select id="modal-form-6" class="form-select" name="upah">
                                    @foreach ($upah as $indx => $rupiah)
                                        @php
                                            $hasil_rupiah = "Rp " . number_format($rupiah->upah,2,',','.');
                                        @endphp
                                        <option value="{{ $rupiah->id }}">{{ $hasil_rupiah }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">NIK</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="modal-form-3" type="text" name="nik" class="form-control" placeholder="NIK...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">NPWP</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="modal-form-3" type="text" name="npwp" class="form-control" placeholder="NPWP...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">TGL Masuk Kerja</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="date" name="tmk" class="form-control">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Nomor Telepon</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="text" name="no_telepon" class="form-control" placeholder="Nomor Telepon...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Jenis Kelamin</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <select id="modal-form-6" class="form-select" name="jenis_kelamin">
                                    <option value="1">Laki-Laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Tempat Lahir</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Tanggal Lahir</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="date" name="tanggal_lahir" class="form-control">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Alamat</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <input id="horizontal-form-2" type="text" name="alamat" class="form-control" placeholder="Alamat Lengkap...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Agama</div>
                                    <div class="ml-2 px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Required</div>
                                </div>
                                <select id="modal-form-6" class="form-select" name="agama">
                                    <option value="Islam">Islam</option>
                                    <option value="Protestan">Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Khonghucu">Khonghucu</option>
                                </select>
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                        
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>

<div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Apakah anda yakin?</div>
                </div>
                <input type="hidden" name="user_id" id="delete-user-id">
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <button type="button" class="btn btn-danger w-24 delete-button">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: Modal Success Delete Content -->
<div id="success-delete-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Terhapus!</div>
                    <div class="text-slate-500 mt-2">Semoga harimu menyenangkan! :D</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Delete Content -->

<!-- BEGIN: Modal Failed Delete Content -->
<div id="failed-delete-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Terhapus!</div>
                    <div class="text-slate-500 mt-2">Ada sesuatu yang salah, Mohon ulangi.</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Delete Content -->
@endsection

@section('js')
@if(Session::has('errors'))
<script>
$(document).ready(function(){
    $('#modal').modal({show: true});
})
</script>
@endif
<script>
    $(".button-success").on('click', function(e){
        e.preventDefault();
        location.reload(true);
    });
    $("#form-data").on('submit', function(e){
        e.preventDefault();
        var data = new FormData($('#form-data')[0]);
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        $.ajax({
            url:"{{ route('MasterUser.store') }}",
            method: 'POST',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success: function(response){
                if (response.errors) {
                    jQuery('.alert-danger').html('');

                    jQuery.each(response.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>'+value+'</li>');
                    });
                    return false;
                }
                if (response.status == 'false') {
                    // return false;

                    $('#registerForm errorList').html(JSON.stringify(response.errors));
                    FormModal.hide();
                    failedModal.show();
                } else {
                    FormModal.hide();
                    successModal.show();
                    
                    // Success notification 
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status);
            }
        });
        return false;
    })

    $(".delete-data").on('click', function(e){
        var delete_id= $(this).data('id');
        $('#delete-user-id').val(delete_id);
    });
    $(".delete-button").on('click', function(e){
        e.preventDefault();
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-delete-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-delete-modal-preview"));
        const deleteModal = tailwind.Modal.getInstance(document.querySelector("#delete-confirmation-modal"));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: `{{ url('masteruser') }}/` + $('#delete-user-id').val() + '/delete',
            data: {
                _method:"DELETE",
                userId: $('#delete-user-id').val()
            },
            success: function(data) {
                deleteModal.hide();
                successModal.show();
            },
            error: function(error) {
                console.log(error)
                deleteModal.hide();
                failedModal.show();
            }
        });
    });
</script>
@endsection