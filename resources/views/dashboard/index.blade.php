@extends('layouts.main')
@section('content')

@section('css')
<style>
    .required:after {
        content:" *";
        color: red;
    }
</style>
@endsection

<div class="flex items-center p-5 border-b border-slate-200/60 mt-5" style="background-color:rgb(98, 174, 23)">
    <h2 class="font-medium text-base mr-auto">
        Selamat Datang, {{ auth()->user()->nama }}! :)
    </h2>
    @if (auth()->user()->role_id == 2)
    <a href="javascript:;" class="ml-auto flex items-center text-secondary button-absen"><i data-lucide="clipboard-check" class="w-4 h-4 mr-3"></i> Absen hari ini</a>
    <input type="hidden" name="" id="jam-mulai-masuk" value="{{ $waktu[0]['jam_mulai'] }}">
    <input type="hidden" name="" id="jam-selesai-masuk" value="{{ $waktu[0]['jam_selesai'] }}">
    <input type="hidden" name="" id="jam-mulai-pulang" value="{{ $waktu[1]['jam_mulai'] }}">
    <input type="hidden" name="" id="jam-selesai-pulang" value="{{ $waktu[1]['jam_selesai'] }}">
    @endif
</div>
{{-- Absen Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="header-footer-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="GET" action="{{route('absen.absen-hari-ini')}}" autocomplete="off">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Absen hari ini
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">E-mail</div>
                                </div>
                                <input id="update-profile-form-1" name="email" type="email" class="form-control" value="{{ auth()->user()->email }}">
                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2 showed-tanggal">
                                    <div class="font-medium required">Tanggal</div>
                                </div>
                                <div class="flex items-center mb-2 hidden-tanggal">
                                    <div class="font-medium required">Tanggal Mulai Cuti</div>
                                </div>
                                <input id="currentDate" name="tanggal" type="date" class="form-control" readonly>
                                @if ($errors->has('tanggal'))
                                <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6 hidden-tanggal-cuti">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Tanggal Selesai Cuti</div>
                                </div>
                                <input id="update-profile-form-1" name="tanggal_selesai_cuti" type="date" class="form-control">
                                @if ($errors->has('tanggal_selesai_cuti'))
                                <span class="text-danger">{{ $errors->first('tanggal_selesai_cuti') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Absen</div>
                                </div>
                                <select name="absen" data-search="true" class="tom-select w-full" id="absen-type">
                                    @foreach($absen as $indeks => $ab)
                                    <option value="{{ $ab->id }}">{{ $ab->absen }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('absen'))
                                <span class="text-danger">{{ $errors->first('absen') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6 jam-control">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Jam</div>
                                </div>
                                <input id="currentTime" name="jam" type="time" class="form-control" readonly>
                                @if ($errors->has('jam'))
                                <span class="text-danger">{{ $errors->first('jam') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Lokasi</div>
                                </div>
                                <select name="lokasi" id="lokasi" class="tom-select w-full">
                                    @foreach($lokasi as $index => $lk)
                                    <option value="{{ $lk->id }}">{{ $lk->lokasi }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('lokasi'))
                                <span class="text-danger">{{ $errors->first('lokasi') }}</span>
                                @endif
                                @if ($errors->has('lainnya'))
                                <span class="text-danger">{{ $errors->first('lainnya') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6 input-hidden">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Lainnya</div>
                                </div>
                                <input type="text" name="lainnya" id="lainnya" class="form-control">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2 showed-keterangan">
                                    <div class="font-medium">Keterangan</div>
                                </div>
                                <div class="flex items-center mb-2 hidden-keterangan">
                                    <div class="font-medium required">Keterangan</div>
                                </div>
                                <input type="text" name="keterangan" id="keterangan" class="form-control">
                                @if ($errors->has('keterangan'))
                                <span class="text-danger">{{ $errors->first('keterangan') }}</span>
                                @endif
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Absen Modal Content --}}

{{-- Null Absen Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="header-footer-modal-preview-null" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="GET" action="{{route('absen.absen-hari-ini')}}" autocomplete="off">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Absen hari ini
                            </h2>
                            <div class="px-2 py-0.5 bg-slate-200 text-slate-600 dark:bg-darkmode-300 dark:text-slate-400 text-xs rounded-md">Anda telah melakukan absen @if($absensi_pulang != null) pulang @endif hari ini.</div>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">E-mail</div>
                                </div>
                                <input id="update-profile-form-1" type="email" class="form-control" value="{{ auth()->user()->email }}" disabled>
                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Tanggal</div>
                                </div>
                                <input id="update-profile-form-1" type="date" class="form-control" disabled>
                                @if ($errors->has('tanggal'))
                                <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Jam</div>
                                </div>
                                <input id="update-profile-form-1"  type="time" class="form-control" disabled>
                                @if ($errors->has('jam'))
                                <span class="text-danger">{{ $errors->first('jam') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Absen</div>
                                </div>
                                <select data-search="true" class="tom-select w-full" id="update-profile-form-2" disabled>
                                    @foreach($absen as $indeks => $ab)
                                    <option value="{{ $ab->id }}">{{ $ab->absen }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('absen'))
                                <span class="text-danger">{{ $errors->first('absen') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Lokasi</div>
                                </div>
                                <select class="tom-select w-full" disabled>
                                    @foreach($lokasi as $index => $lk)
                                    <option value="{{ $lk->id }}">{{ $lk->lokasi }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('lokasi'))
                                <span class="text-danger">{{ $errors->first('lokasi') }}</span>
                                @endif
                                @if ($errors->has('lainnya'))
                                <span class="text-danger">{{ $errors->first('lainnya') }}</span>
                                @endif
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium required">Lainnya</div>
                                </div>
                                <input type="text" class="form-control" disabled>
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Keterangan</div>
                                </div>
                                <input type="text" class="form-control" disabled>
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20" disabled>Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Null Absen Modal Content --}}

<!-- BEGIN: Modal Failed Form Content -->
<div id="failed-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Tersimpan!</div>
                    <div class="text-slate-500 mt-2">
                    @if (session()->has('failed'))
                        {{ session('failed') }}
                    @elseif($errors->has('email') || $errors->has('tanggal')|| $errors->has('jam') || $errors->has('absen') || $errors->has('lokasi') || $errors->has('lainnya') || $errors->has('keterangan')|| $errors->has('tanggal_selesai_cuti'))
                    Data ada yang kurang, Mohon untuk dilengkapi.
                    @else
                        Ada sesuatu yang salah, Mohon ulangi.
                    @endif
                    </div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 ok-failed">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Form Content -->

<!-- BEGIN: Modal Success Form Content -->
<div id="success-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Tersimpan!</div>
                    <div class="text-slate-500 mt-2">
                        @if (session()->has('success'))
                        {{ session('success') }}
                        @endif
                    </div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Form Content -->

@endsection
@section('js')
<script>
    
    $(".ok-failed").on('click', function(e){
        e.preventDefault();
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        FormModal.show();
    });

    @if (session()->has('success'))
    $(document).ready(function(){
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        FormModal.hide();
        successModal.show();
    });
    @endif

    @if (session()->has('failed'))
    $(document).ready(function(){
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        FormModal.hide();
        failedModal.show();
    });
    @endif

    @if ($errors->has('email') || $errors->has('tanggal')|| $errors->has('jam') || $errors->has('absen') || $errors->has('lokasi') || $errors->has('lainnya') || $errors->has('keterangan')|| $errors->has('tanggal_selesai_cuti'))
    $(document).ready(function(){
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        failedModal.show();
    });
    @endif
    
    $(document).ready(function() {
        $('#lokasi').change(function() {
            var option_value = $( "#lokasi option:selected" ).val();
            if (option_value == "4") {
                $(".input-hidden").show();
            }else{
                $(".input-hidden").hide();
            }
        });
        $(".input-hidden").hide();
    });
    
    $(document).ready(function() {
        $('#absen-type').change(function() {
            var option_value = $( "#absen-type option:selected" ).val();
            if (option_value == "4") {
                $(".jam-control").hide();
            }else{
                $(".jam-control").show();
            }

            if (option_value == "6") {
                $(".jam-control").hide();
                $(".showed-tanggal").hide();
                $(".showed-keterangan").hide();
                $("#currentDate").attr("readonly", false); 
                $(".hidden-tanggal-cuti").show();
                $(".hidden-tanggal").show();
                $(".hidden-keterangan").show();
            } else {
                $(".showed-tanggal").show();
                $(".showed-keterangan").show();
                $("#currentDate").attr("readonly", true); 
                $(".hidden-tanggal-cuti").hide();
                $(".hidden-tanggal").hide();
                $(".hidden-keterangan").hide();
                $(".hidden-tanggal").hide();
            }
        });
        $(".hidden-keterangan").hide();
        $(".hidden-tanggal-cuti").hide();
        $(".hidden-tanggal").hide();
        $(".jam-control").show();
    });
    
    $(".button-absen").on('click', function(e){
    e.preventDefault();
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        const FormNullModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview-null"));
        @if($absensi_pulang != null)
        FormNullModal.show();
        @else
        FormModal.show();
        @endif
    });
    

    var date = new Date();
    var currentDate = date.toISOString().substring(0,10);
    var currentTime = moment().format('HH:mm'); 

    document.getElementById('currentDate').value = currentDate;
    document.getElementById('currentTime').value = currentTime;
</script>
@endsection