@extends('layouts.main')

@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    
</div>
<div class="p-5 box">
    <h2 class="mb-5 font-bold text-2xl xl:text-3xl text-center">
        Data Libur Nasional
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <div class="text-center"> <a href="javascript:;" data-tw-toggle="modal" data-tw-target="#header-footer-modal-preview" class="btn btn-primary shadow-md mr-2">Tambah Libur nasional</a></div>
            <div class="text-center"> <a href="javascript:;" data-tw-toggle="modal" data-tw-target="#website-modal-preview" class="btn btn-instagram shadow-md mr-2">Tambah Libur nasional dari website</a></div>
        </div>
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>Tanggal </th>
                        <th>Keterangan </th>
                        <th class="text-center whitespace-nowrap">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($tmlibur as $indeks => $libur)    
                    <tr class="intro-x">
                        <td>{{ $no }}</td>
                        <td>{{ $libur->tanggal }}</td>
                        <td>{{ $libur->keterangan }}</td>
                        <td>
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3 edit-data" href="javascript:;" data-tw-toggle="modal" data-tw-target="#edit-modal-preview" data-id="{{ $libur->id }}" data-tanggal="{{ $libur->tanggal }}" data-keterangan="{{ $libur->keterangan }}"> <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit </a>

                                <a class="flex items-center text-danger delete-data" href="javascript:;" data-id="{{ $libur->id }}" data-tw-toggle="modal" data-tw-target="#delete-confirmation-modal"> <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                            </div>
                        </td>
                    </tr>
                    @php
                        $no++
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
    </div>
    </div>
<!-- END: Delete Confirmation Modal -->
<div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Apakah anda yakin?</div>
                </div>
                <input type="hidden" name="liburnas_id" id="liburnas-delete-id">
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <button type="button" class="btn btn-danger w-24 delete-button">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: Modal Success Delete Content -->
<div id="success-delete-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Terhapus!</div>
                    <div class="text-slate-500 mt-2">Semoga harimu menyenangkan! :D</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Delete Content -->

<!-- BEGIN: Modal Failed Delete Content -->
<div id="failed-delete-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Terhapus!</div>
                    <div class="text-slate-500 mt-2">Ada sesuatu yang salah, Mohon ulangi.</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Delete Content -->

<!-- BEGIN: Modal Failed Form Content -->
<div id="failed-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="x-octagon" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Gagal Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Ada sesuatu yang salah, Mohon ulangi.</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Failed Form Content -->

<!-- BEGIN: Modal Success Form Content -->
<div id="success-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center"> <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Data Tersimpan!</div>
                    <div class="text-slate-500 mt-2">Semoga harimu menyenangkan! :D</div>
                </div>
                <div class="px-5 pb-8 text-center"> <button type="button" data-tw-dismiss="modal" class="btn btn-primary w-24 button-success">Ok</button> </div>
            </div>
        </div>
    </div>
</div> 
<!-- END: Modal Success Form Content -->

{{-- Tambah Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="header-footer-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form id="form-data">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Tambah Libur Nasional
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Tanggal</div>
                                </div>
                                <input id="modal-form-2" type="date" name="tanggal" class="form-control" placeholder="Tanggal...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Keterangan</div>
                                </div>
                                <input id="modal-form-2" type="text" name="keterangan" class="form-control" placeholder="Keterangan...">
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Tambah Modal Content --}}

{{-- Tambah from website Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="website-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form id="form-data-webliburnas">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Tambah Libur Nasional dari website {{ date('Y') }}
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body">
                            <table class="table table-report">
                                <thead>
                                    <tr>
                                        <th>NO.</th>
                                        <th>Tanggal </th>
                                        <th>Keterangan </th>
                                        <th class="text-center whitespace-nowrap">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no=1;
                                    @endphp
                                    @foreach ($liburnas as $indeks => $libur)    
                                    <tr class="intro-x">
                                        <td>{{ $no }}</td>
                                        <td>{{ $libur[0] }}</td>
                                        <td>{{ $libur[2] }}</td>
                                        <td>
                                            <div class="flex justify-center items-center">
                                                <input class="form-check-input" type="checkbox" name="libur[{{ $indeks }}]" id="">
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $no++
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Tambah from website Modal Content --}}

{{-- Edit Modal content --}}
<div id="header-footer-modal" class="p-5">
    <div class="preview">
        <!-- BEGIN: Modal Content -->
        <div id="edit-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form id="form-edit">
                    @csrf
                    <!-- BEGIN: Modal Header -->
                        <div class="modal-header">
                            <h2 class="font-medium text-base mr-auto">
                                Edit Libur Nasional
                            </h2>
                        </div>
                        <!-- END: Modal Header -->
                        <!-- BEGIN: Modal Body -->
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Tanggal</div>
                                </div>
                                <input type="hidden" name="liburnas_id" id="liburnas-id" class="form-control">
                                <input type="date" name="tanggal" id="tanggal" class="form-control" placeholder="Tanggal...">
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <div class="flex items-center mb-2">
                                    <div class="font-medium">Keterangan</div>
                                </div>
                                <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan...">
                            </div>
                        </div>
                        <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-20">Send</button>
                        </div>
                    </form>
                    <!-- END: Modal Footer -->
                </div>
            </div>
        </div>
        <!-- END: Modal Content -->
    </div>
</div>
{{-- END: Edit Modal Content --}}
@endsection

@section('js')

<script>
    $(".delete-data").on('click', function(e){
        var delete_id= $(this).data('id');
        $('#liburnas-delete-id').val(delete_id);
    });
    
    $(".edit-data").on('click', function(e){
        var liburnas_id= $(this).data('id');
        var tanggal= $(this).data('tanggal');
        var keterangan= $(this).data('keterangan');
        console.log(liburnas_id);
        console.log(tanggal);
        console.log(keterangan);
        $('#liburnas-id').val(liburnas_id);
        $('#tanggal').val(tanggal);
        $('#keterangan').val(keterangan);
    });
    
    $(".button-success").on('click', function(e){
        e.preventDefault();
        location.reload(true);
    });

    $(".delete-button").on('click', function(e){
        e.preventDefault();
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-delete-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-delete-modal-preview"));
        const deleteModal = tailwind.Modal.getInstance(document.querySelector("#delete-confirmation-modal"));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: `{{ url('Liburnas/') }}/` + $('#liburnas-delete-id').val() + '/delete',
            data: {
                _method:"DELETE",
                liburnasId: $('#liburnas-delete-id').val()
            },
            success: function(data) {
                deleteModal.hide();
                successModal.show();
            },
            error: function(error) {
                console.log(error)
                deleteModal.hide();
                failedModal.show();
            }
        });
    });
    
    $("#form-data").on('submit', function(e){
        e.preventDefault();
        var data = new FormData($('#form-data')[0]);
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        $.ajax({
            url:"{{ route('Liburnas.store') }}",
            method: 'POST',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success: function(response){
                if (response.errors) {
                    jQuery('.alert-danger').html('');

                    jQuery.each(response.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>'+value+'</li>');
                    });
                    return false;
                }
                if (response.status == 'false') {
                    // return false;

                    $('#registerForm errorList').html(JSON.stringify(response.errors));
                    FormModal.hide();
                    failedModal.show();
                } else {
                    // Success notification 
                    FormModal.hide();
                    successModal.show();
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status);
            }
        });
        return false;
    })

    $("#form-edit").on('submit', function(e){
        e.preventDefault();
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const editModal = tailwind.Modal.getInstance(document.querySelector("#edit-modal-preview"));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('Liburnas.updateLibur') }}",
            data: {
                _method:"POST",
                id: $('#liburnas-id').val(),
                tanggal: $('#tanggal').val(),
                keterangan: $('#keterangan').val(),
            },
            success: function(response) {
                if (response.errors) {
                    jQuery('.alert-danger').html('');

                    jQuery.each(response.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>'+value+'</li>');
                    });
                    return false;
                }
                if (response.status == 'false') {
                    // return false;
                    $('#registerForm errorList').html(JSON.stringify(response.errors));
                    editModal.hide();
                    failedModal.show();
                } else {
                    // Success notification 
                    editModal.hide();
                    successModal.show();
                }
            },
            error: function(error) {
                console.log(error)
                editModal.hide();
                failedModal.show();
            }
        });
    })

    $("#form-data-webliburnas").on('submit', function(e){
        e.preventDefault();
        var data = new FormData($('#form-data-webliburnas')[0]);
        const successModal = tailwind.Modal.getInstance(document.querySelector("#success-modal-preview"));
        const failedModal = tailwind.Modal.getInstance(document.querySelector("#failed-modal-preview"));
        const FormModal = tailwind.Modal.getInstance(document.querySelector("#header-footer-modal-preview"));
        $.ajax({
            url:"{{ route('Liburnas.storeLiburnasFromWebsite') }}",
            method: 'POST',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success: function(response){
                if (response.errors) {
                    jQuery('.alert-danger').html('');

                    jQuery.each(response.errors, function(key, value){
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>'+value+'</li>');
                    });
                    return false;
                }
                if (response.status == 'false') {
                    // return false;

                    $('#registerForm errorList').html(JSON.stringify(response.errors));
                    FormModal.hide();
                    failedModal.show();
                } else {
                    // Success notification 
                    FormModal.hide();
                    successModal.show();
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status);
            }
        });
        return false;
    })
</script>
@endsection