@extends('layouts.app')

@section('content')

    
        <!-- BEGIN: Login Info -->
        <div class="hidden xl:flex flex-col min-h-screen">
            <a href="" class="-intro-x flex items-center pt-5">
                <img alt="Midone - HTML Admin Template" class="logo__image w-20" src="{{ url('assets/dist/images/invensi.png') }}">
                <span class="text-white text-lg ml-3"> Invensi.id </span> 
            </a>
            <div class="my-auto">
                <img alt="Midone - HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="{{ url('assets/dist/images/illustration.svg') }}">
                <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                    We'll make IT simple  
                    <br>
                    for you.
                </div>
                <div class="-intro-x mt-5 text-lg text-white text-opacity-70 dark:text-slate-400">Don't Let IT complicates you, let us assist to make IT simple 
                <br>
                and best as a solution.</div>
            </div>
        </div>
        <!-- END: Login Info -->
        <!-- BEGIN: Login Form -->
        <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
            <div class="my-auto mx-auto xl:ml-20 bg-white dark:bg-darkmode-600 xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                    Log In
                </h2>
                @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if (session()->has('loginError'))
                <div class="alert alert-danger">
                    {{ session('loginError') }}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="form form-login"  method="POST" action="{{ route('login') }}">
                    @csrf
                    
                    <div class="intro-x mt-8">
                        
                        <input name="email" id="login-email" type="email" class="intro-x login__input form-control py-3 px-4 block" placeholder="Email" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="intro-x login__input form-control py-3 px-4 block mt-4" placeholder="Password" id="login-password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong class="text-danger">{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    {{-- <div class="intro-x flex text-slate-600 dark:text-slate-500 text-xs sm:text-sm mt-4">
                        <div class="flex items-center mr-auto">
                            <input id="remember-me" type="checkbox" class="form-check-input border mr-2">
                            <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
                        </div>
                        <a href="">Forgot Password?</a> 
                    </div> --}}
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <button type="submit" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">Login</button>
                        {{-- <button class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">Register</button> --}}
                    </div>
                </form>
            </div>
        </div>
        <!-- END: Login Form -->

    {{-- <div class="forms">
        <div class="form-wrapper row mb-3 is-active">
            <button type="button" class="switcher switcher-login">
                Login
                <span class="underline"></span>
            </button>
            <form class="form form-login"  method="POST" action="{{ route('login') }}">
                @csrf
                <fieldset>
                    <legend>Please, enter your email and password for login.</legend>
                <div class="input-block">
                    <label for="login-email">E-mail</label>
                    <input name="email" id="login-email" type="email" required>
                </div>
                <div class="input-block">
                    <label for="login-password">Password</label>
                    <input id="login-password" name="password" type="password" required>
                </div>
                </fieldset>
                <button type="submit" class="btn-login">Login</button>
            </form>
        </div>
        <div class="form-wrapper row mb-3">
        <button type="button" class="switcher switcher-signup">
            Absen Hari ini
            <span class="underline"></span>
        </button>
        
    </div> --}}
@endsection

@push('js')
<script>
    $(document).ready(function() {
        $('#lokasi').change(function() {
            var option_value = $( "#lokasi option:selected" ).val();
            if (option_value == "4") {
                $(".input-hidden").show();
            }else{
                $(".input-hidden").hide();
            }
        });
        $(".input-hidden").hide();
    });
</script>
@endpush
