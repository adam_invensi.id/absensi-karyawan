<?php

use App\Http\Controllers\DepartemenController;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\JamKerjaController;
use App\Http\Controllers\LiburnasController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hashpassword', function () {
    print_r(Hash::make('Secret123!@#'));
    die();
    // return ;
});

Route::get('/hashingpassword', function () {
    return Hash::make('test123');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function(){
    Route::get('/absensi/absensi-hari-ini/AbsenHariIni', 'App\Http\Controllers\Absensi\AbsensiHariIniController@absenHariIni')->name('absen.absen-hari-ini');
    //Dashboard
    Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');
    //AUTH
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    //Jam Lembur
    Route::get('/JamJembur/pengajuan-lembur/{id}/create', 'App\Http\Controllers\PengajuanLemburController@create')->name('Lembur.create');
    
    //Absensi
    Route::get('/absensi/absensi-hari-ini/index', 'App\Http\Controllers\Absensi\AbsensiHariIniController@index')->name('Absen.index');
    Route::get('/absensi/absensi-hari-ini/{id}/show', 'App\Http\Controllers\Absensi\AbsensiHariIniController@show')->name('Absen.show');
    Route::post('/absensi/absensi-hari-ini/filter_by_month', 'App\Http\Controllers\Absensi\AbsensiHariIniController@filter_by_month')->name('Absen.filter-by-month');
    Route::get('/absensi/absensi-hari-ini/show_old', 'App\Http\Controllers\Absensi\AbsensiHariIniController@rv')->name('Absen.show-old');
    Route::get('/absensi/absensi-hari-ini/show_ol', 'App\Http\Controllers\Absensi\AbsensiHariIniController@show_old')->name('absen.absen-hari-ini-ol');
    Route::get('/absensi/absensi-hari-ini/exportExcelAll', 'App\Http\Controllers\Absensi\AbsensiHariIniController@exportExcelAll')->name('Absen.export-excel-all');
    Route::get('/absensi/absensi-hari-ini/exportExcelUser', 'App\Http\Controllers\Absensi\AbsensiHariIniController@exportExcelUser')->name('Absen.export-excel-user');
    //Holiday
    Route::get('/absensi/absensi-hari-ini/getHolidays', 'App\Http\Controllers\Absensi\AbsensiHariIniController@getHolidays')->name('Absen.get-holidays');
    
    //Report
    Route::get('/absensi/report-absensi/index', 'App\Http\Controllers\Absensi\ReportAbsensiController@index')->name('Report.index');
    Route::post('/absensi/report-absensi/exportReport', 'App\Http\Controllers\Absensi\ReportAbsensiController@exportReport')->name('Report.export-report');
    Route::post('/absensi/report-absensi/importReport', 'App\Http\Controllers\Absensi\ReportAbsensiController@importReport')->name('Report.import-report');
    Route::post('/absensi/report-absensi/getDownload', 'App\Http\Controllers\Absensi\ReportAbsensiController@getDownload')->name('Report.get-download');

    //Master User
    Route::resource('/MasterUser', App\Http\Controllers\MasterUserController::class);
    Route::get('/MasterUser/PengaturanAkun/{id}', 'App\Http\Controllers\MasterUserController@pengaturanAkun')->name('MasterUser.pengaturan-akun');
    Route::get('/MasterUser/PengaturanPengguna/{id}', 'App\Http\Controllers\MasterUserController@pengaturanPengguna')->name('MasterUser.pengaturan-pengguna');
    Route::post('/MasterUser/updatePengaturanPengguna', 'App\Http\Controllers\MasterUserController@updatePengaturanPengguna')->name('MasterUser.update-pengaturan-pengguna');
    Route::post('/MasterUser/gantiFoto', 'App\Http\Controllers\MasterUserController@gantiFoto')->name('MasterUser.ganti-foto');
    Route::get('/MasterUser/changePassword/{id}', 'App\Http\Controllers\MasterUserController@changePassword')->name('MasterUser.change-password');
    Route::post('/MasterUser/updatePassword', 'App\Http\Controllers\MasterUserController@updatePassword')->name('MasterUser.update-password');

    //Liburnas
    Route::resource('/Liburnas', App\Http\Controllers\LiburnasController::class);
    Route::post('Liburnas/store-liburnas-from-website', [LiburnasController::class, 'storeLiburnasFromWebsite'])->name('Liburnas.storeLiburnasFromWebsite');
    Route::post('Liburnas/update-libur', [LiburnasController::class, 'updateLibur'])->name('Liburnas.updateLibur');
    Route::delete('Liburnas/{id_liburnas}/delete', [LiburnasController::class, 'delete']);


    //Jam Kerja
    Route::resource('/JamKerja', App\Http\Controllers\JamKerjaController::class);
    Route::post('JamKerja/update-jam', [JamKerjaController::class, 'updateJam'])->name('Jam.updateJam');
    
    //DEPARTEMEN
    Route::resource('/Departemen', App\Http\Controllers\DepartemenController::class);
    Route::delete('departemen/{id_departemen}/delete', [DepartemenController::class, 'delete']);
    Route::post('departemen/update-departemen', [DepartemenController::class, 'updateDepartemen'])->name('Departemen.updateDepartemen');
    
    //JABATAN
    Route::resource('/Jabatan', App\Http\Controllers\JabatanController::class);
    Route::delete('jabatan/{id_jabatan}/delete', [JabatanController::class, 'delete']);
    Route::post('jabatan/update-jabatan', [JabatanController::class, 'updateJabatan'])->name('Jabatan.updateJabatan');
});